module.exports = {
  parser: '@typescript-eslint/parser',
  parserOptions: {
    project: 'tsconfig.json',
    sourceType: 'module',
  },
  plugins: ['@typescript-eslint/eslint-plugin'],
  extends: [
    'plugin:@typescript-eslint/recommended',
    'plugin:prettier/recommended',
  ],
  root: true,
  env: {
    node: true,
    jest: true,
  },
  ignorePatterns: ['.eslintrc.js'],
  rules: {
    '@typescript-eslint/explicit-module-boundary-types': 'off',
    '@typescript-eslint/interface-name-prefix': 'off',
    '@typescript-eslint/explicit-function-return-type': 'off',
    '@typescript-eslint/explicit-module-boundary-types': 'off',
    '@typescript-eslint/no-explicit-any': 'off',
    '@typescript-eslint/no-unused-vars': ['error'],

    'no-var-requires': 0,

    '@typescript-eslint/ban-types': ['error', { types: { object: false } }],

    'no-unused-vars': 'off',

    'no-restricted-imports': [
      'error',
      {
        patterns: [
          {
            group: ['.*'],
            message: `usage of relative imports is forbidden - use '@app' or '@shared' prefix`,
          },
        ],
      },
    ],

    'no-restricted-syntax': [
      'error',
      {
        selector:
          'CallExpression[callee.property.name="save"] MemberExpression[property.name="offersRepository"], CallExpression[callee.property.name="update"] MemberExpression[property.name="offersRepository"]',
        message:
          'Use OffersService for saving and updating offers instead of calling the methods on the repository directly.',
      },
    ],
  },
}
