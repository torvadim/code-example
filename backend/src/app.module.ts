import { Module } from '@nestjs/common'
import { ConfigModule, ConfigService } from '@nestjs/config'
import { ScheduleModule } from '@nestjs/schedule'
import { EventEmitterModule } from '@nestjs/event-emitter'
import { MongooseModule } from '@nestjs/mongoose'
import { ChangeRatesModule } from '@app/changeRates/changeRates.module'
import { EnvVariables } from '@common/enums'
import { AuthModule } from '@app/auth/auth.module'
import { UsersModule } from '@app/users/users.module'
import { JwtAccessTokenGuard } from '@app/auth/jwt-access-token.guard'
import { APP_GUARD } from '@nestjs/core'

@Module({
  providers: [
    ConfigService,
    {
      provide: APP_GUARD,
      useClass: JwtAccessTokenGuard,
    },
  ],
  imports: [
    ConfigModule.forRoot(),
    MongooseModule.forRootAsync({
      imports: [ConfigModule],
      useFactory: async (configService: ConfigService) => ({
        uri: configService.get<string>(EnvVariables.MongoDbUri),
        autoIndex: true,
      }),
      inject: [ConfigService],
    }),
    ScheduleModule.forRoot(),
    EventEmitterModule.forRoot(),
    AuthModule,
    UsersModule,
    ChangeRatesModule,
  ],
})
export class AppModule {}
