import { Response } from 'express'
import { produce } from 'immer'
import dayjs from 'dayjs'
import { Model } from 'mongoose'
import { InjectModel } from '@nestjs/mongoose'

import {
  Controller,
  Post,
  UseGuards,
  HttpCode,
  Body,
  UnauthorizedException,
  Res,
  Req,
  UnprocessableEntityException,
} from '@nestjs/common'

import { AuthService } from '@app/auth/auth.service'
import { Auth, AuthDocument } from '@app/auth/auth.schema'
import { JwtRefreshTokenGuard } from '@app/auth/jwt-refresh-token.guard'
import { RequestWithUser } from '@app/auth/jwt-refresh-token.strategy'
import { CookieNames, EnvVariables } from '@common/enums'
import { ConfigService } from '@nestjs/config'
import { NodeEnvironments } from '@common/enums'
import { Public } from '@app/auth/jwt-access-token.guard'

import { AuthLoginDto, LogoutFromAllSessionsDto } from '@app/auth/auth.dto'
import { UsersService } from '@app/users/domain/users.service'
import { validatePassword } from '@src/common/utils/validatePassword'

const getIsUserIdInDecodedToken = (
  decodedToken: string | { [key: string]: any },
): decodedToken is { id: number } => !!(decodedToken as { id: number }).id

@Public()
@Controller('auth')
export class AuthController {
  constructor(
    @InjectModel(Auth.name)
    private readonly authRepository: Model<AuthDocument>,
    private readonly authService: AuthService,
    private readonly configService: ConfigService,
    private readonly usersService: UsersService,
  ) {}

  private get refreshCookieOptions() {
    return {
      httpOnly: true,
      secure:
        this.configService.get(EnvVariables.NodeEnv) ===
        NodeEnvironments.Production,
    }
  }

  @Post('/login')
  async login(
    @Res({ passthrough: true }) response: Response,
    @Body() body: AuthLoginDto,
  ) {
    const user = await this.usersService.findUserByLogin(body.login)

    const isPasswordValid = await validatePassword(
      body.password,
      user?.passwordHash,
    )

    if (!user || !isPasswordValid) {
      throw new UnauthorizedException('Invalid login or password')
    }

    const payload = { id: user.id }

    const [accessToken, refreshToken] = await Promise.all([
      this.authService.getAccessToken(payload),
      this.authService.getRefreshToken(payload),
    ])

    await this.authRepository.create({
      user_id: user.id,
      refresh_token: refreshToken,
      is_revoked: false,
    })

    response.cookie(CookieNames.RefreshToken, refreshToken, {
      httpOnly: true,
      secure:
        this.configService.get(EnvVariables.NodeEnv) ===
        NodeEnvironments.Production,
    })

    return { access_token: accessToken }
  }

  @UseGuards(JwtRefreshTokenGuard)
  @Post('/refresh')
  async refresh(
    @Req() request: RequestWithUser,
    @Res({ passthrough: true }) response: Response,
  ) {
    const payload = { id: request.user.id }

    const isRefreshTokenExpired =
      request.user.refreshTokenExpiresAt < dayjs().unix()

    const basePromises = [this.authService.getAccessToken(payload)]

    const [accessToken, refreshToken] = await Promise.all(
      produce(basePromises, (draft) => {
        if (isRefreshTokenExpired) {
          draft.push(this.authService.getRefreshToken(payload))
        }
      }),
    )

    if (isRefreshTokenExpired) {
      await Promise.all([
        this.authRepository.updateOne(
          { refresh_token: request.cookies.refresh_token },
          { is_revoked: true },
        ),
        this.authRepository.create({
          user_id: request.user.id,
          refresh_token: refreshToken,
          is_revoked: false,
        }),
      ])

      response.cookie(
        CookieNames.RefreshToken,
        refreshToken,
        this.refreshCookieOptions,
      )
    }

    return { access_token: accessToken }
  }

  @UseGuards(JwtRefreshTokenGuard)
  @Post('/logout')
  @HttpCode(200)
  async logout(
    @Req() request: RequestWithUser,
    @Res({ passthrough: true }) response: Response,
  ) {
    await this.authRepository.create(
      { refresh_token: request.cookies.refresh_token },
      { is_revoked: true },
    )

    response.clearCookie(CookieNames.RefreshToken, this.refreshCookieOptions)
  }

  @Post('/logout-from-all-sessions')
  @HttpCode(200)
  async logoutFromAllSessions(@Body() { token }: LogoutFromAllSessionsDto) {
    await this.authService.verifyToken(token)

    const decodedToken = this.authService.decodeToken(token)

    if (!getIsUserIdInDecodedToken(decodedToken)) {
      throw new UnprocessableEntityException('Failed to retrieve user data')
    }

    await this.authRepository.create(
      { user_id: decodedToken.id },
      { is_revoked: true },
    )
  }
}
