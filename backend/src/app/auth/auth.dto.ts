import { IsNotEmptyString } from '@common/utils/IsNotEmptyString'

export class AuthLoginDto {
  @IsNotEmptyString()
  login: string

  @IsNotEmptyString()
  password: string
}

export class LogoutFromAllSessionsDto {
  @IsNotEmptyString()
  token: string
}
