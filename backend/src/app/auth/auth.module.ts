import { Module } from '@nestjs/common'
import { PassportModule } from '@nestjs/passport'
import { JwtModule } from '@nestjs/jwt'
import { ConfigModule, ConfigService } from '@nestjs/config'

import { AuthService } from '@app/auth/auth.service'
import { AuthController } from '@app/auth/auth.controller'
import { JwtAccessTokenStrategy } from '@app/auth/jwt-access-token.strategy'
import { JwtRefreshTokenStrategy } from '@app/auth/jwt-refresh-token.strategy'
import { Auth, AuthSchema } from '@app/auth/auth.schema'
import { EnvVariables } from '@common/enums'
import { MongooseModule } from '@nestjs/mongoose'
import { UsersModule } from '@app/users/users.module'

@Module({
  imports: [
    ConfigModule,

    PassportModule,

    UsersModule,

    MongooseModule.forFeature([{ name: Auth.name, schema: AuthSchema }]),

    JwtModule.registerAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: async (configService: ConfigService) => ({
        secret: configService.get(EnvVariables.JwtAccessTokenSecret),
        signOptions: {
          expiresIn: configService.get(EnvVariables.JwtAccessTokenExpiration),
        },
      }),
    }),
  ],
  controllers: [AuthController],
  providers: [AuthService, JwtAccessTokenStrategy, JwtRefreshTokenStrategy],
  exports: [AuthService],
})
export class AuthModule {}
