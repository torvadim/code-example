import { User } from '@app/users/domain/user.schema'
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose'
import mongoose, { Document } from 'mongoose'

@Schema()
export class Auth {
  @Prop({ type: mongoose.Schema.Types.ObjectId, ref: 'User' })
  user?: User

  @Prop()
  userId: number

  @Prop()
  refreshToken: string

  @Prop()
  isRevoked: boolean
}

export type AuthDocument = Auth & Document

export const AuthSchema = SchemaFactory.createForClass(Auth)
