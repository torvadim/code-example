import { Injectable } from '@nestjs/common'
import { JwtService } from '@nestjs/jwt'
import { ConfigService } from '@nestjs/config'

import { EnvVariables } from '@common/enums'

@Injectable()
export class AuthService {
  constructor(
    private readonly jwtService: JwtService,
    private readonly configService: ConfigService,
  ) {}

  getAccessToken<T extends object>(payload: T) {
    const accessTokenSecret = this.configService.get(
      EnvVariables.JwtAccessTokenSecret,
    )

    const accessTokenExpiration = this.configService.get(
      EnvVariables.JwtAccessTokenExpiration,
    )

    return this.jwtService.signAsync(payload, {
      secret: accessTokenSecret,
      expiresIn: accessTokenExpiration,
    })
  }

  getRefreshToken<T extends object>(payload: T) {
    const refreshTokenSecret = this.configService.get(
      EnvVariables.JwtRefreshTokenSecret,
    )

    const refreshTokenExpiration = this.configService.get(
      EnvVariables.JwtRefreshTokenExpiration,
    )

    return this.jwtService.signAsync(payload, {
      secret: refreshTokenSecret,
      expiresIn: refreshTokenExpiration,
    })
  }

  getResetPasswordToken<T extends object>(payload: T) {
    const resetPasswordTokenSecret = this.configService.get(
      EnvVariables.JwtResetPasswordTokenSecret,
    )

    const resetPasswordTokenExpiration = this.configService.get(
      EnvVariables.JwtResetPasswordTokenExpiration,
    )

    return this.jwtService.signAsync(payload, {
      secret: resetPasswordTokenSecret,
      expiresIn: resetPasswordTokenExpiration,
    })
  }

  verifyToken(token: string) {
    return this.jwtService.verifyAsync(token)
  }

  decodeToken(token: string) {
    return this.jwtService.decode(token)
  }
}
