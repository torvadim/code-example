import { Injectable, ExecutionContext, SetMetadata } from '@nestjs/common'
import { AuthGuard } from '@nestjs/passport'
import { Reflector } from '@nestjs/core'

import { JWT_ACCESS_TOKEN_STRATEGY_NAME } from '@app/auth/jwt-access-token.strategy'

const IS_PUBLIC_KEY = 'isPublic'

@Injectable()
export class JwtAccessTokenGuard extends AuthGuard(JWT_ACCESS_TOKEN_STRATEGY_NAME) {
  constructor(private readonly reflector: Reflector) {
    super()
  }

  canActivate(context: ExecutionContext) {
    const isPublic = this.reflector.getAllAndOverride<boolean>(IS_PUBLIC_KEY, [context.getHandler(), context.getClass()])

    if (isPublic) {
      return true
    }

    return super.canActivate(context)
  }
}

export const Public = () => SetMetadata(IS_PUBLIC_KEY, true)
