import { ExtractJwt, Strategy } from 'passport-jwt'
import { PassportStrategy } from '@nestjs/passport'
import { Injectable } from '@nestjs/common'
import { ConfigService } from '@nestjs/config'

import { EnvVariables } from '@common/enums'

export const JWT_ACCESS_TOKEN_STRATEGY_NAME = 'jwt'

interface Payload {
  id: string
  exp: number
  iat: number
}

@Injectable()
export class JwtAccessTokenStrategy extends PassportStrategy(Strategy, JWT_ACCESS_TOKEN_STRATEGY_NAME) {
  constructor(private readonly configService: ConfigService) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      secretOrKey: configService.get(EnvVariables.JwtAccessTokenSecret),
      ignoreExpiration: false,
    })
  }

  validate(payload: Payload) {
    return { id: payload.id }
  }
}
