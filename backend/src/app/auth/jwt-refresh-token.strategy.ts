import { PassportStrategy } from '@nestjs/passport'
import { Injectable, UnauthorizedException } from '@nestjs/common'
import { ConfigService } from '@nestjs/config'
import { ExtractJwt, Strategy } from 'passport-jwt'
import { Request } from 'express'
import { Model } from 'mongoose'
import { InjectModel } from '@nestjs/mongoose'

import { Auth, AuthDocument } from '@app/auth/auth.schema'

export const JWT_REFRESH_TOKEN_STRATEGY_NAME = 'jwt-refresh-token'

interface Payload {
  id: number
  exp: number
  iat: number
}

export interface RequestUser {
  id: string
  refreshTokenExpiresAt: number
}

export interface RequestWithUser extends Request {
  user: RequestUser
}

@Injectable()
export class JwtRefreshTokenStrategy extends PassportStrategy(
  Strategy,
  JWT_REFRESH_TOKEN_STRATEGY_NAME,
) {
  constructor(
    @InjectModel(Auth.name)
    private readonly authRepository: Model<AuthDocument>,
    private readonly configService: ConfigService,
  ) {
    super({
      jwtFromRequest: ExtractJwt.fromExtractors([
        (request) => request.cookies?.refresh_token,
      ]),
      secretOrKey: configService.get('JWT_REFRESH_TOKEN_SECRET'),
      ignoreExpiration: true,
      passReqToCallback: true,
    })
  }

  async validate(request: Request, payload: Payload) {
    const refreshToken = request.cookies?.refresh_token

    const authEntity = await this.authRepository.findOne({
      userId: payload.id,
      refreshToken,
    })

    if (!authEntity || authEntity.isRevoked) {
      throw new UnauthorizedException()
    }

    return {
      id: payload.id,
      refreshTokenExpiresAt: payload.exp,
    }
  }
}
