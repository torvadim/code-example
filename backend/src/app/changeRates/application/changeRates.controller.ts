import { Controller, Get } from '@nestjs/common'

import { ChangeRatesService } from '@app/changeRates/domain/changeRates.service'

@Controller('rates')
export class ChangeRatesController {
  constructor(private changeRateService: ChangeRatesService) {}

  @Get('/')
  async getRates() {
    return this.changeRateService.findAll()
  }

  @Get('/imports')
  async rumImports() {
    return this.changeRateService.importRates()
  }
}
