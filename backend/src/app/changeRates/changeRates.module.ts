import { Module } from '@nestjs/common'
import { MongooseModule } from '@nestjs/mongoose'
import { ConfigModule } from '@nestjs/config'

import { ChangeRatesController } from '@app/changeRates/application/changeRates.controller'
import { ChangeRatesService } from '@app/changeRates/domain/changeRates.service'
import { ChangeRate, ChangeRateSchema } from '@app/changeRates/domain/changeRate.schema'

@Module({
  imports: [ConfigModule, MongooseModule.forFeature([{ name: ChangeRate.name, schema: ChangeRateSchema }])],
  controllers: [ChangeRatesController],
  providers: [ChangeRatesService],
})
export class ChangeRatesModule {}
