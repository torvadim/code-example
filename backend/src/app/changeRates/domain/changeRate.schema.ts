import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose'
import { Document } from 'mongoose'

@Schema()
export class ChangeRate {
  @Prop()
  country: string

  @Prop()
  currency: string

  @Prop()
  amount: number

  @Prop()
  code: string

  @Prop()
  rate: string

  @Prop()
  dateImported: Date
}

export type ChangeRateDocument = ChangeRate & Document

export const ChangeRateSchema = SchemaFactory.createForClass(ChangeRate)
