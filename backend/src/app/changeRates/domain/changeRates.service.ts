import axios from 'axios'
import { Model } from 'mongoose'
import { Injectable } from '@nestjs/common'
import { InjectModel } from '@nestjs/mongoose'
import { ConfigService } from '@nestjs/config'

import {
  ChangeRate,
  ChangeRateDocument,
} from '@app/changeRates/domain/changeRate.schema'

@Injectable()
export class ChangeRatesService {
  constructor(
    @InjectModel(ChangeRate.name)
    private changeRateModel: Model<ChangeRateDocument>,
    private readonly configService: ConfigService,
  ) {}

  parseData(data: string): ChangeRate[] {
    const array = data.split('\n')

    array.splice(0, 2)

    const rates = array
      .filter((val) => val.includes('|'))
      .map((item): ChangeRate => {
        const data = item.split('|')

        return {
          country: data[0],
          currency: data[1],
          amount: Number(data[2]),
          code: data[3],
          rate: data[4],
          dateImported: new Date(),
        }
      })

    return rates
  }

  async importRates() {
    const url = this.configService.get('CNB_URL')

    const { data } = await axios.get(url)

    const rates = this.parseData(data)

    return this.changeRateModel.insertMany(rates)
  }

  async findAll(): Promise<ChangeRate[]> {
    return this.changeRateModel.find().exec()
  }
}
