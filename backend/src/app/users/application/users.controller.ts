import { Body, Controller, Delete, Get, Post, Req } from '@nestjs/common'

import { Public } from '@app/auth/jwt-access-token.guard'
import { RequestWithUser } from '@app/auth/jwt-refresh-token.strategy'
import { UsersService } from '@app/users/domain/users.service'
import { UsersRegisterDto } from '@app/users/application/users.dto'

@Controller('user')
export class UsersController {
  constructor(private userService: UsersService) {}

  @Get('me')
  async me(@Req() request: RequestWithUser) {
    return this.userService.findUserById(request.user.id)
  }

  @Public()
  @Post('register')
  async register(@Body() registerDto: UsersRegisterDto) {
    await this.userService.createUser(registerDto.login, registerDto.password)

    return 'ok'
  }

  @Delete('delete')
  async delete(@Req() request: RequestWithUser) {
    return this.userService.deleteUser(request.user.id)
  }
}
