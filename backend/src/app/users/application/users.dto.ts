import { IsNotEmptyString } from '@common/utils/IsNotEmptyString'

export class UsersRegisterDto {
  @IsNotEmptyString()
  login: string

  @IsNotEmptyString()
  password: string
}
