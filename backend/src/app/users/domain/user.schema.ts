import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose'

@Schema()
export class User {
  @Prop({ unique: true })
  login: string

  @Prop()
  passwordHash: string
}

export type UserDocument = User & Document

export const UserSchema = SchemaFactory.createForClass(User)
