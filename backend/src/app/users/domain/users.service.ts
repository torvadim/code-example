import { Model } from 'mongoose'
import { InjectModel } from '@nestjs/mongoose'
import * as argon2 from 'argon2'

import { UserDocument, User } from '@app/users/domain/user.schema'

export class UsersService {
  constructor(@InjectModel(User.name) private userModel: Model<UserDocument>) {}

  async findUserById(id: string) {
    return this.userModel.findOne({ id })
  }

  async findUserByLogin(login: string) {
    return this.userModel.findOne({ login })
  }

  async createUser(login: string, password: string) {
    const passwordHash = await argon2.hash(password, { type: argon2.argon2id })

    return this.userModel.create({
      login,
      passwordHash,
    })
  }

  async updateUsersPassword(id: string, password: string) {
    const passwordHash = await argon2.hash(password, { type: argon2.argon2id })

    await this.userModel.updateOne({ id }, { passwordHash })
  }

  async deleteUser(id: string) {
    return this.userModel.deleteOne({ id })
  }
}
