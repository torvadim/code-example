export enum NodeEnvironments {
  Development = 'development',
  Production = 'production',
}

export enum EnvVariables {
  BusinessEnv = 'BUSINESS_ENV',
  AppPort = 'APP_PORT',
  MongoDbUri = 'MONGODB_URI',
  NodeEnv = 'NODE_ENV',
  JwtAccessTokenSecret = 'JWT_ACCESS_TOKEN_SECRET',
  JwtAccessTokenExpiration = 'JWT_ACCESS_TOKEN_EXPIRATION_TIME',
  JwtRefreshTokenSecret = 'JWT_REFRESH_TOKEN_SECRET',
  JwtRefreshTokenExpiration = 'JWT_REFRESH_TOKEN_EXPIRATION_TIME',
  JwtResetPasswordTokenSecret = 'JWT_RESET_PASSWORD_TOKEN_SECRET',
  JwtResetPasswordTokenExpiration = 'JWT_RESET_PASSWORD_TOKEN_EXPIRATION_TIME',
}

export enum CookieNames {
  RefreshToken = 'refresh_token',
}
