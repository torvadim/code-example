import * as argon2 from 'argon2'

export async function validatePassword(
  password: string,
  passwordHash: string,
): Promise<boolean> {
  if (passwordHash) {
    return argon2.verify(passwordHash, password, {
      type: argon2.argon2id,
    })
  }

  return false
}
