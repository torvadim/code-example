/* eslint-disable no-console */
import { NestFactory } from '@nestjs/core'
import { ValidationPipe } from '@nestjs/common'
import * as cookieParser from 'cookie-parser'

import { AppModule } from '@src/app.module'
const port = process.env.PORT ? parseInt(process.env.PORT, 10) : 4000

async function bootstrap() {
  const app = await NestFactory.create(AppModule)
  app.useGlobalPipes(new ValidationPipe())

  app.enableCors()

  app.use(cookieParser())

  await app.listen(port)
  console.log(`> App started http://localhost:${port}`)
}
bootstrap()
