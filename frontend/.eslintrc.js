module.exports = {
  extends: [
    'plugin:prettier/recommended',
    'plugin:@typescript-eslint/recommended',
  ],

  env: {
    browser: true,
  },

  globals: {
    JSX: true,
    process: true,
  },

  plugins: ['@typescript-eslint'],

  ignorePatterns: ['.eslintrc.js'],

  rules: {
    'no-undef': 'error',

    '@typescript-eslint/explicit-module-boundary-types': 'off',

    'no-unused-vars': 'off',

    'no-var-requires': 'off',

    '@typescript-eslint/no-unused-vars': ['error'],

    '@typescript-eslint/ban-types': ['error', { types: { object: false } }],

    '@typescript-eslint/no-empty-interface': [
      'error',
      { allowSingleExtends: true },
    ],

    'no-restricted-imports': [
      'error',
      {
        patterns: [
          {
            group: ['.*'],
            message: `usage of relative imports is forbidden - use '@app' or '@shared' prefix`,
          },
        ],
      },
    ],
  },
}
