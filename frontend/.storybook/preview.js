import styled from 'styled-components'
import { StylesProvider } from '@material-ui/core/styles'
import { ThemeProvider } from 'styled-components'
import { ThemeProvider as MuiThemeProvider } from '@material-ui/core/styles'
import AppProvider from '../src/components/providers/AppProvider'


import { theme } from '../src/theme'

const Container = styled.div`
  align-items: center;
  display: flex;
  justify-content: center;
`

const withMaterialUI = (Story, context) => {
  return (
    <StylesProvider injectFirst>
      <Story {...context} />
    </StylesProvider>
  )
}

const withContainer = (Story, context) => {
  return (
    <Container>
      <Story {...context} />
    </Container>
  )
}
const withAppProvider = (Story, context) => {
  return (
    <AppProvider theme={theme}>
      <Story {...context} />
    </AppProvider>
  )
}

export const decorators = [
  withMaterialUI,
  withAppProvider,
  withContainer,
]

export const parameters = {
  layout: 'fullscreen',
  actions: { argTypesRegex: '^on[A-Z].*' },
  controls: {
    matchers: {
      color: /(background|color)$/i,
      date: /Date$/,
    },
  },
}
