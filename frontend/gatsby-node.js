const path = require('path')

// @see https://www.hoshki.me/blog/2020-08-22-using-gatsby-like-an-spa/
exports.onCreatePage = ({ page, actions }) => {
  const { createPage } = actions

  if (page.path === '/') {
    page.matchPath = '/*'

    createPage(page)
  }
}

exports.onCreateWebpackConfig = function ({ actions }) {
  actions.setWebpackConfig({
    resolve: {
      alias: {
        '@app': path.resolve(__dirname, 'src/'),
      },
    },
  })
}
