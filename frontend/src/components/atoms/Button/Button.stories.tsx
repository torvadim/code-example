import React, { useState } from 'react'
import { ComponentStory, ComponentMeta } from '@storybook/react'
import styled from 'styled-components'

import Box from '@material-ui/core/Box'
import ArrowUpIcon from '@material-ui/icons/KeyboardArrowUp'
import ArrowDownIcon from '@material-ui/icons/KeyboardArrowDown'

import Button from '@app/components/atoms/Button/Button'
import Logo from '@app/components/atoms/Logo/Logo'
import { theme } from '@app/theme'
import Tooltip from '@app/components/atoms/Tooltip/Tooltip'

export default {
  title: '1 Atoms/Button',
  argTypes: { onClick: { action: 'clicked' } },
  parameters: {
    docs: {
      source: {
        type: 'code',
      },
      description: {
        component:
          'Extends [Material UI Button component](https://material-ui.com/components/buttons/)\n\n' +
          'Additional props are drilled to [Button](https://material-ui.com/api/button/) component',
      },
    },
  },
  component: Button,
} as ComponentMeta<typeof Button>

export const Basic: ComponentStory<typeof Button> = (props) => {
  return <Button {...props}>Button</Button>
}

export const Loading: ComponentStory<typeof Button> = (props) => {
  const onButtonClick = (): Promise<void> =>
    new Promise((resolve) => setTimeout(resolve, 2500))

  return (
    <Button {...props} onClick={onButtonClick}>
      Do something async
    </Button>
  )
}

export const RoundWithIcon: ComponentStory<typeof Button> = (props) => {
  const StyledLogo = styled(Logo)`
    color: ${({ theme }) => theme.palette.common.white};
    padding-right: 0.5rem;
  `

  const StyledButton = styled(Button)`
    width: 9rem;
  `

  return (
    <StyledButton {...props} round>
      <StyledLogo width="30px" color={theme.palette.common.white} />
      Add prices
    </StyledButton>
  )
}

export const WithTooltip: ComponentStory<typeof Button> = (props) => {
  const [isTooltipOpen, setIsTooltipOpen] = useState(false)

  const TooltipContent = () => {
    return (
      <StyledTooltipWrapper>
        <StyledTooltipText>
          Change status to quoted without sending any message to the customer.
        </StyledTooltipText>
        <StyledButton {...props}>Mark as quoted</StyledButton>
      </StyledTooltipWrapper>
    )
  }

  return (
    <Box minHeight="10rem">
      <Tooltip
        interactive
        backgroundColor={theme.palette.common.white}
        borderColor={theme.palette.grey[300]}
        color={theme.palette.common.black}
        onClose={() => setIsTooltipOpen(false)}
        title={<TooltipContent />}
        open={isTooltipOpen}
      >
        <Box display="flex" justifyContent="center" flexDirection="column">
          <StyledButton
            {...props}
            onClick={() => setIsTooltipOpen(!isTooltipOpen)}
          >
            <StyledSpan>Send quotation</StyledSpan>
            <ArrowWrapper>
              {isTooltipOpen ? <ArrowUpIcon /> : <ArrowDownIcon />}
            </ArrowWrapper>
          </StyledButton>
        </Box>
      </Tooltip>
    </Box>
  )
}

const StyledButton = styled(Button)`
  background-color: ${({ theme }) => theme.colors.purple};
  padding: 0.5rem 0.5rem 0.5rem 1rem;
  width: 100%;

  &:hover {
    background-color: ${({ theme }) => theme.colors.purple};
  }
`

const StyledSpan = styled.span`
  border-right: 1px solid ${({ theme }) => theme.palette.common.white};
  color: ${({ theme }) => theme.palette.common.white};
  padding-right: 0.5rem;
`

const ArrowWrapper = styled.span`
  margin-left: 0.5rem;
  display: flex;
`

const StyledTooltipWrapper = styled.div`
  padding: 0.5rem;
  max-width: 9rem;
`

const StyledTooltipText = styled.div`
  margin-bottom: 1rem;
`
