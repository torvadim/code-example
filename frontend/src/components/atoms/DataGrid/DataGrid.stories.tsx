import React, { useMemo, useState } from 'react'
import { ComponentStory, ComponentMeta } from '@storybook/react'
import { CellProps } from 'react-table'

import Typography from '@app/components/atoms/Typography/Typography'

import DataGrid, {
  DataGridColumn,
} from '@app/components/atoms/DataGrid/DataGrid'

export default {
  title: '1 Atoms/DataGrid',
  component: DataGrid,
  parameters: {
    docs: {
      source: {
        type: 'code',
      },
      description: {
        component:
          'Extends [Material UI Table component](https://material-ui.com/components/tables/), [React Table](https://react-table.tanstack.com/) and [react-infinite-scroll-component](https://github.com/ankeetmaini/react-infinite-scroll-component#readme)\n\n' +
          'Additional props are drilled to root div element',
      },
    },
  },
} as ComponentMeta<typeof DataGrid>

export const Basic: ComponentStory<typeof DataGrid> = (props) => {
  interface Item {
    firstName: string
    lastName: string
    age?: number
  }

  const AgeCell = useMemo(() => {
    const Cell = ({ cell }: CellProps<Item>) => (
      <Typography>{cell.row.original.age ?? 'unknown'}</Typography>
    )

    return Cell
  }, [])

  const FullNameCell = useMemo(() => {
    const Cell = ({ cell }: CellProps<Item>) => (
      <Typography>
        {cell.row.original.firstName} {cell.row.original.lastName}
      </Typography>
    )

    return Cell
  }, [])

  const columns = useMemo(
    () => [
      { title: 'First name', accessor: 'firstName' },
      { title: 'Last name', accessor: 'lastName' },
      { title: 'Full name', id: 'fullName', Cell: FullNameCell },
      { title: 'Age', id: 'age', Cell: AgeCell },
    ],
    [FullNameCell, AgeCell],
  )

  const data = useMemo(
    () => [
      { firstName: 'Jon', lastName: 'Snow', age: 35 },
      { firstName: 'Cersei', lastName: 'Lannister', age: 42 },
      { firstName: 'Jaime', lastName: 'Lannister', age: 45 },
      { firstName: 'Arya', lastName: 'Stark', age: 16 },
      { firstName: 'Daenerys', lastName: 'Targaryen' },
    ],
    [],
  )

  return (
    <DataGrid
      {...props}
      columns={props.columns ?? columns}
      data={props.data ?? data}
    />
  )
}

export const Sortable: ComponentStory<typeof DataGrid> = (props) => {
  interface Item {
    firstName: string
    lastName: string
    age?: number
  }

  const AgeCell = useMemo(() => {
    const Cell = ({ cell }: CellProps<Item>) => (
      <Typography>{cell.row.original.age ?? 'unknown'}</Typography>
    )

    return Cell
  }, [])

  const FullNameCell = useMemo(() => {
    const Cell = ({ cell }: CellProps<Item>) => (
      <Typography>
        {cell.row.original.firstName} {cell.row.original.lastName}
      </Typography>
    )

    return Cell
  }, [])

  const columns = useMemo<DataGridColumn<Item>[]>(
    () => [
      { title: 'First name', accessor: 'firstName', isSortable: true },
      { title: 'Last name', accessor: 'lastName' },
      {
        title: 'Full name',
        id: 'fullName',
        Cell: FullNameCell,
        isSortable: true,
      },
      { title: 'Age', id: 'age', Cell: AgeCell, isSortable: true },
    ],
    [FullNameCell, AgeCell],
  )

  const [data, setData] = useState<Item[]>([
    { firstName: 'Jon', lastName: 'Snow', age: 35 },
    { firstName: 'Cersei', lastName: 'Lannister', age: 42 },
    { firstName: 'Jaime', lastName: 'Lannister', age: 45 },
    { firstName: 'Arya', lastName: 'Stark', age: 16 },
    { firstName: 'Daenerys', lastName: 'Targaryen' },
  ])

  const [initialData] = useState(data)

  return (
    <DataGrid
      {...props}
      columns={props.columns ?? columns}
      data={props.data ?? data}
      onSortChange={async (sort) => {
        if (!sort) {
          setData(initialData)

          return
        }

        const value = sort.direction === 'desc' ? 1 : -1

        switch (sort.id) {
          case 'fullName': {
            await new Promise((resolve) => setTimeout(resolve, 2000))

            setData((prevData) =>
              prevData
                .concat()
                .sort((a, b) =>
                  `${a.firstName} ${a.lastName}` >
                  `${b.firstName} ${b.lastName}`
                    ? value
                    : -value,
                ),
            )

            break
          }

          case 'firstName': {
            setData((prevData) =>
              prevData
                .concat()
                .sort((a, b) => (a.firstName > b.firstName ? value : -value)),
            )

            break
          }

          case 'age': {
            setData((prevData) =>
              prevData
                .concat()
                .sort((a, b) =>
                  !a.age || !b.age ? 1 : a.age > b.age ? value : -value,
                ),
            )

            break
          }

          default:
            console.warn(`Sort id '${sort.id}' is unhandled!`)
        }
      }}
    />
  )
}

export const Exportable: ComponentStory<typeof DataGrid> = (props) => {
  interface Item {
    firstName: string
    lastName: string
    age?: number
  }

  const AgeCell = useMemo(() => {
    const Cell = ({ cell }: CellProps<Item>) => (
      <Typography>{cell.row.original.age ?? 'unknown'}</Typography>
    )

    return Cell
  }, [])

  const FullNameCell = useMemo(() => {
    const Cell = ({ cell }: CellProps<Item>) => (
      <Typography>
        {cell.row.original.firstName} {cell.row.original.lastName}
      </Typography>
    )

    return Cell
  }, [])

  const columns = useMemo(
    () => [
      { title: 'First name', accessor: 'firstName' },
      { title: 'Last name', accessor: 'lastName' },
      { title: 'Full name', id: 'fullName', Cell: FullNameCell },
      { title: 'Age', id: 'age', Cell: AgeCell },
    ],
    [FullNameCell, AgeCell],
  )

  const data = useMemo(
    () => [
      { firstName: 'Jon', lastName: 'Snow', age: 35 },
      { firstName: 'Cersei', lastName: 'Lannister', age: 42 },
      { firstName: 'Jaime', lastName: 'Lannister', age: 45 },
      { firstName: 'Arya', lastName: 'Stark', age: 16 },
      { firstName: 'Daenerys', lastName: 'Targaryen' },
    ],
    [],
  )

  return (
    <DataGrid
      {...props}
      columns={props.columns ?? columns}
      data={props.data ?? data}
    />
  )
}

export const InfiniteScroll: ComponentStory<typeof DataGrid> = (props) => {
  interface Item {
    firstName: string
    lastName: string
    age?: number
  }

  const AgeCell = useMemo(() => {
    const Cell = ({ cell }: CellProps<Item>) => (
      <Typography>{cell.row.original.age ?? 'unknown'}</Typography>
    )

    return Cell
  }, [])

  const FullNameCell = useMemo(() => {
    const Cell = ({ cell }: CellProps<Item>) => (
      <Typography>
        {cell.row.original.firstName} {cell.row.original.lastName}
      </Typography>
    )

    return Cell
  }, [])

  const columns = useMemo(
    () => [
      { title: 'First name', accessor: 'firstName' },
      { title: 'Last name', accessor: 'lastName' },
      { title: 'Full name', id: 'fullName', Cell: FullNameCell },
      { title: 'Age', id: 'age', Cell: AgeCell },
    ],
    [FullNameCell, AgeCell],
  )

  const [data, setData] = useState(() => [
    { firstName: 'Jon', lastName: 'Snow', age: 35 },
    { firstName: 'Cersei', lastName: 'Lannister', age: 42 },
    { firstName: 'Jaime', lastName: 'Lannister', age: 45 },
    { firstName: 'Arya', lastName: 'Stark', age: 16 },
    { firstName: 'Daenerys', lastName: 'Targaryen' },
  ])

  return (
    <DataGrid
      {...props}
      columns={props.columns ?? columns}
      data={props.data ?? data}
      hasMore={data.length < 20}
      onNextDataRequested={async () => {
        await new Promise((resolve) => setTimeout(resolve, 2000))

        setData((prevData) => prevData.concat(prevData.slice(0, 5)))
      }}
    />
  )
}
