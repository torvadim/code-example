import React, { useMemo, ReactNode, Fragment } from 'react'
import styled from 'styled-components'
import InfiniteScroll from 'react-infinite-scroll-component'

import {
  useRowSelect,
  useTable,
  ColumnInstance,
  CellProps as BaseCellProps,
  Column as BaseColumn,
} from 'react-table'

import Table from '@material-ui/core/Table'
import Collapse from '@material-ui/core/Collapse'
import TableBody from '@material-ui/core/TableBody'
import TableCell from '@material-ui/core/TableCell'
import TableContainer from '@material-ui/core/TableContainer'
import TableHead from '@material-ui/core/TableHead'
import TableRow from '@material-ui/core/TableRow'
import TableSortLabel from '@material-ui/core/TableSortLabel'
import Grid from '@material-ui/core/Grid'
import CircularProgress from '@material-ui/core/CircularProgress'
import { OrderDirection } from '@app/utils/constants'

import Typography from '@app/components/atoms/Typography/Typography'

interface CellProps<T extends object> extends BaseCellProps<T> {
  column: ColumnInstance<T> & { title: string }
}

type Sort = {
  id: string
  direction:
    | Lowercase<typeof OrderDirection.Desc>
    | Lowercase<typeof OrderDirection.Asc>
}

export type DataGridColumn<T extends object> = BaseColumn<T> & {
  title?: string
  isSortable?: boolean
}

export interface DataGridProps<T extends object> {
  columns: BaseColumn<T>[]
  data: T[]
  cellProps?: Record<string, unknown>
  hasMore?: boolean
  className?: string
  collapsedRowId?: number | null
  getCollapsedContainer?: (collapsedRowId: number | null) => ReactNode
  onNextDataRequested?: () => void
  sort?: Sort | null
  defaultSortDirection?: 'asc' | 'desc'
  onSortChange?: (nextSort: Sort | null) => void
}

export const DATA_GRID_DATA_ROW_ID_ATTRIBUTE = 'data-row-id'

function DataGrid<T extends { id: number }>({
  columns,
  data,
  cellProps,
  className,
  onNextDataRequested,
  collapsedRowId = null,
  getCollapsedContainer,
  sort,
  onSortChange,
  hasMore = false,

  defaultSortDirection = 'desc',
  ...props
}: DataGridProps<T>): JSX.Element {
  const { getTableProps, headers, prepareRow, rows } = useTable<T>(
    {
      defaultColumn: {
        Cell: DefaultBodyCell,
        Header: DefaultHeaderCell,
        width: 0,
      },
      data,
      columns,
    },
    useRowSelect,
  )

  const updateActiveSort = async (columnId: string) => {
    let nextDirection: Sort['direction'] | null = null

    if (sort?.id !== columnId) {
      nextDirection = defaultSortDirection
    }

    if (sort?.id === columnId && sort.direction === defaultSortDirection) {
      nextDirection = defaultSortDirection === 'desc' ? 'asc' : 'desc'
    }

    const nextSort =
      nextDirection === null
        ? null
        : {
            direction: nextDirection,
            id: columnId,
          }

    onSortChange?.(nextSort)
  }

  const collapsedContainer = useMemo(() => {
    if (!collapsedRowId) {
      return
    }

    return getCollapsedContainer?.(collapsedRowId)
  }, [collapsedRowId])

  return (
    <DataGridContainer className={className} {...props}>
      <InfiniteScroll
        hasMore={hasMore}
        dataLength={rows.length}
        next={() => onNextDataRequested?.()}
        loader={<Loader />}
      >
        <TableContainer>
          <Table {...getTableProps()}>
            <TableHead>
              <DataGridHeadRow>
                {headers.map((column) => (
                  <DataGridHeadCell
                    {...column.getHeaderProps({
                      style: {
                        minWidth: column.minWidth,
                        width: column.width || 'initial',
                      },
                    })}
                    key={column.id}
                    onClick={() => {
                      if (column.isSortable) {
                        updateActiveSort(column.id)
                      }
                    }}
                  >
                    {column.render('Header')}
                    {column.id !== 'selection' && column.isSortable && (
                      <SortContainer>
                        <TableSortLabel
                          direction={sort?.direction ?? defaultSortDirection}
                        />
                      </SortContainer>
                    )}
                  </DataGridHeadCell>
                ))}
              </DataGridHeadRow>
            </TableHead>
            <TableBody>
              {rows.map((row, rowIndex) => {
                prepareRow(row)

                return (
                  <Fragment key={rowIndex}>
                    <TableRow
                      {...row.getRowProps()}
                      {...{
                        [DATA_GRID_DATA_ROW_ID_ATTRIBUTE]: row.original.id,
                      }}
                    >
                      {row.cells.map((cell, cellIndex) => {
                        return (
                          <DataGridCell
                            {...cell.getCellProps({
                              style: {
                                minWidth: cell.column.minWidth,
                                width: cell.column.width || 'initial',
                              },
                            })}
                            key={cellIndex}
                          >
                            {cell.render('Cell', {
                              data: row.original,
                              ...cellProps,
                            })}
                          </DataGridCell>
                        )
                      })}
                    </TableRow>
                    {collapsedContainer && (
                      <TableRow>
                        <CollapsedContainerCell
                          style={{ border: 0, paddingBottom: 0, paddingTop: 0 }}
                          colSpan={6}
                        >
                          <Collapse
                            in={row.original.id === collapsedRowId}
                            timeout="auto"
                            unmountOnExit
                          >
                            {row.original.id === collapsedRowId &&
                              collapsedContainer}
                          </Collapse>
                        </CollapsedContainerCell>
                      </TableRow>
                    )}
                  </Fragment>
                )
              })}
            </TableBody>
          </Table>
        </TableContainer>
      </InfiniteScroll>
    </DataGridContainer>
  )
}

const Loader = () => (
  <DataGridLoaderContainer
    container
    alignItems="center"
    justifyContent="center"
  >
    <CircularProgress size={20} />
  </DataGridLoaderContainer>
)

const DefaultHeaderCell = <T extends object>({
  column,
}: CellProps<T>): JSX.Element => {
  return <HeaderTypography variant="title">{column.title}</HeaderTypography>
}

const DefaultBodyCell = <T extends object>({
  cell,
}: CellProps<T>): JSX.Element => {
  return <Typography variant="content">{cell.value}</Typography>
}

export const SimpleCell = <T extends object>({
  cell,
}: CellProps<T>): JSX.Element => {
  const columnId = cell.column.id

  return (
    <Typography variant="content">
      {cell.row.original[columnId as keyof T] ?? '-'}
    </Typography>
  )
}

const HeaderTypography = styled(Typography)`
  display: inline;
`

const SortContainer = styled.div`
  position: relative;
  display: inline;
`

const DataGridContainer = styled.div`
  position: relative;
  width: 100%;

  & > div {
    width: inherit;
  }
`

const DataGridHeadCell = styled(TableCell)`
  padding-top: 0;
  padding-bottom: 0.25rem;
`

const DataGridHeadRow = styled(TableRow)`
  height: 3rem;
`

const CollapsedContainerCell = styled(TableCell)`
  padding: 0;
`

const DataGridCell = styled(TableCell)`
  white-space: nowrap;
`

const DataGridLoaderContainer = styled(Grid)`
  height: 3rem;
`

export default DataGrid
