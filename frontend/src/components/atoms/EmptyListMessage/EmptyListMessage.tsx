import React from 'react'
import styled from 'styled-components'

import Typography from '@app/components/atoms/Typography/Typography'
import Button from '@app/components/atoms/Button/Button'

interface EmptyListMessageProps {
  title?: string
  description?: string
  ctaOptions?: {
    onClick: () => void
    buttonText: string
  }
  className?: string
}

const EmptyListMessage = ({
  description,
  ctaOptions,
  className,
  ...props
}: EmptyListMessageProps) => {
  const title = props.title ?? 'No data was found'

  return (
    <Container className={className}>
      <StyledTypography variant="boldText">{title}</StyledTypography>
      {description && (
        <StyledTypography variant="subtitle">{description}</StyledTypography>
      )}
      {ctaOptions && (
        <StyledButton inverted onClick={ctaOptions.onClick}>
          {ctaOptions.buttonText}
        </StyledButton>
      )}
    </Container>
  )
}

const Container = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  max-width: 300px;
  text-align: center;
`

const StyledTypography = styled(Typography)`
  margin-bottom: 0.5rem;
`

const StyledButton = styled(Button)`
  margin-top: 1rem;
`

export default EmptyListMessage
