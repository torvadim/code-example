import React from 'react'
import { ComponentStory, ComponentMeta } from '@storybook/react'

import Box from '@material-ui/core/Box'

import LoadingSpinner from '@app/components/atoms/LoadingSpinner/LoadingSpinner'
import Logo from '@app/components/atoms/Logo/Logo'
import Typography from '@app/components/atoms/Typography/Typography'

export default {
  title: '1 Atoms/LoadingSpinner',
  component: LoadingSpinner,
  parameters: {
    docs: {
      source: {
        type: 'code',
      },
      description: {
        component:
          'Extends [Material UI CircularProgress component](https://next.material-ui.com/components/progress/)\n\n' +
          'Additional props are drilled to [CircularProgress](https://material-ui.com/api/circular-progress) component',
      },
    },
  },
} as ComponentMeta<typeof LoadingSpinner>

export const Basic: ComponentStory<typeof LoadingSpinner> = ({ ...props }) => {
  return (
    <Box my={5}>
      <LoadingSpinner {...props} loading={props.loading ?? true}>
        <Typography>Code example</Typography>
      </LoadingSpinner>
    </Box>
  )
}

export const WithLogo: ComponentStory<typeof LoadingSpinner> = (props) => {
  return (
    <Box mt={5}>
      <LoadingSpinner {...props} loading={props.loading ?? true}>
        <Logo />
      </LoadingSpinner>
    </Box>
  )
}
