import React from 'react'
import { ComponentStory, ComponentMeta } from '@storybook/react'

import SimpleButton from '@app/components/atoms/SimpleButton/SimpleButton'

export default {
  title: '1 Atoms/SimpleButton',
  component: SimpleButton,
  argTypes: { onClick: { action: 'clicked' } },
  parameters: {
    docs: {
      source: {
        type: 'code',
      },
      description: {
        component: 'Extends HTML Button element',
      },
    },
  },
} as ComponentMeta<typeof SimpleButton>

export const Basic: ComponentStory<typeof SimpleButton> = (props) => (
  <SimpleButton {...props}>Simple button</SimpleButton>
)
