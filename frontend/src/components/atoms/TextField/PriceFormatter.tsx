import React, { ReactElement } from 'react'
import NumberFormat from 'react-number-format'

interface NumberFormatCustomProps {
  inputRef: (instance: NumberFormat<string> | null) => void
  onChange: (event: { target: { name: string; value: string } }) => void
  name: string
}

const PriceFormatter = (props: NumberFormatCustomProps): ReactElement => {
  const { inputRef, onChange, ...rest } = props

  return (
    <NumberFormat
      {...rest}
      suffix={`CZK`}
      thousandSeparator=" "
      isNumericString
      getInputRef={inputRef}
      onValueChange={(values) => {
        onChange({
          target: {
            name: props.name,
            value: values.value,
          },
        })
      }}
    />
  )
}

// @see https://v4.mui.com/components/text-fields/#integration-with-3rd-party-input-libraries
// eslint-disable-next-line
export default PriceFormatter as any
