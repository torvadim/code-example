import React, { ChangeEvent, useState } from 'react'
import { ComponentStory, ComponentMeta } from '@storybook/react'
import styled from 'styled-components'

import InputAdornment from '@material-ui/core/InputAdornment'
import SearchIcon from '@material-ui/icons/Search'

import ErrorTooltip from '@app/components/atoms/Tooltip/ErrorTooltip'

import TextField, {
  TextFieldPriceFormatter,
} from '@app/components/atoms/TextField/TextField'

export default {
  title: '1 Atoms/TextField',
  component: TextField,
  parameters: {
    docs: {
      source: {
        type: 'code',
      },
      description: {
        component:
          'Extends [Material UI TextField component](https://material-ui.com/components/text-fields/)\n\n' +
          'Additional props are drilled to [InputBase](https://material-ui.com/api/input-base/) component',
      },
    },
  },
} as ComponentMeta<typeof TextField>

export const WithLabel: ComponentStory<typeof TextField> = ({
  label = 'Label of TextFields',
  id = 'textFieldWithLabel',
  ...props
}) => <TextField {...props} label={label} id={id} placeholder="Placeholder" />

export const Password: ComponentStory<typeof TextField> = ({
  label = 'Label of TextField',
  type = 'password',
  id = 'textFieldPassword',
  ...props
}) => {
  return <TextField {...props} label={label} type={type} id={id} />
}

export const Multiline: ComponentStory<typeof TextField> = ({
  multiline = true,
  rows = 6,
  placeholder = 'Type your message here ...',
  ...props
}) => {
  const StyledTextField = styled(TextField)`
    width: 25rem;
  `

  return (
    <StyledTextField
      {...props}
      multiline={multiline}
      rows={rows}
      placeholder={placeholder}
    />
  )
}

export const StartAdornment: ComponentStory<typeof TextField> = (props) => {
  return <TextField {...props} startAdornment={<SearchIcon />} />
}

export const EndAdornment: ComponentStory<typeof TextField> = (props) => {
  const StyledTextField = styled(TextField)`
    width: 5rem;

    & .TextField__input {
      text-align: right;
    }
  `

  return (
    <StyledTextField
      {...props}
      endAdornment={<InputAdornment position="end">€</InputAdornment>}
      classes={{
        input: 'TextField__input',
      }}
    />
  )
}

export const WithPriceFormatter: ComponentStory<typeof TextField> = (props) => {
  return <TextField {...props} inputComponent={TextFieldPriceFormatter} />
}

export const Validation: ComponentStory<typeof TextField> = (props) => {
  const [isError, setIsError] = useState<boolean>(true)
  const [value, setValue] = useState('')

  const onTextFieldChange = (event: ChangeEvent<HTMLInputElement>) => {
    const { value } = event.target

    setValue(value)
    setIsError(value.length < 10)
  }

  return (
    <Container>
      <ErrorTooltip
        open={isError}
        title="TextField must be at least 10 characters long"
        placement="bottom"
      >
        <TextField
          {...props}
          error={isError}
          value={value}
          onChange={onTextFieldChange}
          label={props.label ?? 'Label of TextFields'}
        />
      </ErrorTooltip>
    </Container>
  )
}

const Container = styled.div`
  padding: 2rem;
`
