import React, { useState } from 'react'
import styled from 'styled-components'
import { ComponentStory, ComponentMeta } from '@storybook/react'

import ErrorIcon from '@material-ui/icons/Error'
import InfoIcon from '@material-ui/icons/Info'

import InputBase from '@material-ui/core/InputBase'

import Tooltip from '@app/components/atoms/Tooltip/Tooltip'
import ErrorTooltip from '@app/components/atoms/Tooltip/ErrorTooltip'

export default {
  title: '1 Atoms/Tooltip',
  component: Tooltip,
  parameters: {
    docs: {
      source: {
        type: 'code',
      },
      description: {
        component:
          'Extends [Material UI Tooltip component](https://material-ui.com/components/tooltips/)\n\n' +
          'Additional props are drilled to [Tooltip](https://material-ui.com/api/tooltip/) component',
      },
    },
  },
} as ComponentMeta<typeof Tooltip>

export const Basic: ComponentStory<typeof Tooltip> = ({
  title = 'Basic tooltip',
  placement = 'top',
  disabled,
  ...props
}) => {
  return (
    <Container>
      <Tooltip
        title={title}
        placement={placement}
        disabled={disabled}
        {...props}
      >
        <InfoIcon />
      </Tooltip>
    </Container>
  )
}

export const Complex: ComponentStory<typeof Tooltip> = ({
  placement = 'right',
  disabled,
  ...props
}) => {
  const [isOpen, setIsOpen] = useState(false)

  const TooltipContent = () => {
    const [value, setValue] = useState('')

    return (
      <>
        <div>Václav Havel Airport</div>
        <div>Prague, Czech Republic</div>
        <StyledInputBase
          value={value}
          onChange={(event) => {
            setValue(event.target.value)
          }}
          type="text"
          onFocusCapture={() => console.log('test')}
          onTouchStart={() => console.log('touch')}
        />
      </>
    )
  }

  return (
    <Container>
      <Tooltip
        placement={placement}
        {...props}
        onClose={() => setIsOpen(false)}
        open={props.open ?? isOpen}
        title={props.title ?? <TooltipContent />}
        interactive={props.interactive ?? true}
        disabled={disabled}
      >
        <InfoIcon onClick={() => setIsOpen(true)} />
      </Tooltip>
    </Container>
  )
}

export const BasicErrorTooltip: ComponentStory<typeof Tooltip> = (props) => {
  return (
    <Container>
      <ErrorTooltip {...props} title={props.title ?? 'Example error title'}>
        <StyledErrorIcon />
      </ErrorTooltip>
    </Container>
  )
}

const StyledErrorIcon = styled(ErrorIcon)`
  color: ${({ theme }) => theme.palette.error.main};
`

const Container = styled.div`
  padding: 2rem;
`

const StyledInputBase = styled(InputBase)`
  border-radius: 3px;
  padding: 0.5rem;
  border: 1px solid grey;
  margin-top: 1.5rem;
  background-color: white;
`
