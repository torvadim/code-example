import React from 'react'
import { ComponentMeta } from '@storybook/react'

import Grid from '@material-ui/core/Grid'
import Box from '@material-ui/core/Box'

import Typography from '@app/components/atoms/Typography/Typography'

export const TypographyVariants = (): JSX.Element => {
  return (
    <Grid container direction="column">
      <Box my={1}>
        <Typography variant="heading">Heading typography</Typography>
      </Box>
      <Box my={1}>
        <Typography variant="dialogHeading">Dialog heading</Typography>
      </Box>

      <Box my={1}>
        <Typography>Default typography</Typography>
      </Box>
      <Box my={1}>
        <Typography variant="secondary">Secondary typography</Typography>
      </Box>
      <Box my={1}>
        <Typography variant="content">Content typography</Typography>
      </Box>

      <Box my={1}>
        <Typography variant="title">Title typography</Typography>
      </Box>
      <Box my={1}>
        <Typography variant="subtitle">Subtitle typography</Typography>
      </Box>
      <Box my={1}>
        <Typography variant="scheduleLabel">
          Schedule label typography
        </Typography>
      </Box>
      <Box my={1}>
        <Typography variant="smallDarkText">Small Dark Text</Typography>
      </Box>
    </Grid>
  )
}

export default {
  title: '1 Atoms/Typography',
  component: Typography,
  parameters: {
    docs: {
      source: {
        type: 'code',
      },
      description: {
        component:
          'Extends [Material UI Typography component](https://material-ui.com/components/typography/)\n\n' +
          'Additional props are drilled to [Typography](https://material-ui.com/api/typography/) component',
      },
    },
  },
} as ComponentMeta<typeof TypographyVariants>
