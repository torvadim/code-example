import React from 'react'
import styled from 'styled-components'

import BaseTypography, {
  TypographyProps as BaseTypographyProps,
} from '@material-ui/core/Typography'

type Variants =
  | 'default'
  | 'secondary'
  | 'title'
  | 'heading'
  | 'subtitle'
  | 'content'
  | 'mainHeading'
  | 'formTitle'
  | 'formSectionTitle'
  | 'userTitle'
  | 'boldText'
  | 'sectionTitle'
  | 'label'
  | 'dialogHeading'
  | 'scheduleLabel'
  | 'smallDarkText'
  | 'errorText'

export interface TypographyProps extends Omit<BaseTypographyProps, 'variant'> {
  variant?: Variants
}

interface StyledTypographyProps extends Omit<BaseTypographyProps, 'variant'> {
  $variant: TypographyProps['variant']
}

const Typography = ({
  variant = 'default',
  ...props
}: TypographyProps): JSX.Element => (
  <StyledTypography $variant={variant} {...props} />
)

const StyledTypography = styled(BaseTypography)<StyledTypographyProps>`
  ${({ $variant, theme }) => {
    switch ($variant) {
      case 'secondary':
        return `
          color: ${theme.palette.grey[700]};
          font-family: ${theme.font.secondary};
          font-size: 1rem;
          font-weight: 400;`

      case 'content':
        return `
          color: ${theme.palette.grey[800]};
          font-family: ${theme.font.secondary};
          font-size: 0.875rem;
          font-weight: 400;`

      case 'mainHeading':
        return `
          color: ${theme.text.heading.regular};
          font-family: ${theme.font.primary};
          font-size: 1.75rem;
          font-weight: 500;`

      case 'title':
        return `
          color: ${theme.palette.grey[600]};
          font-family: ${theme.font.secondary};
          font-size: 0.625rem;
          font-weight: 400;`

      case 'subtitle':
        return `
          color: ${theme.palette.grey[500]};
          font-family: ${theme.font.secondary};
          font-size: 0.75rem;
          font-weight: 400`

      case 'heading':
        return `
          color: ${theme.text.heading.regular};
          font-family: ${theme.font.primary};
          font-size: 2.25rem;
          font-weight: 500;`

      case 'formTitle':
        return `
          color: ${theme.text.heading.regular};
          font-size: 1rem;
          font-weight: 600;
          margin-bottom: 1rem;
          `

      case 'boldText':
        return `font-weight: 800;`

      case 'label':
        return `
          font-size: 0.75rem;
          color: ${theme.text.heading.dark};
        `

      case 'smallDarkText':
        return `
          color: ${theme.palette.grey[800]};
          font-size: 0.75rem;
        `

      case 'errorText':
        return `
          color: ${theme.colors.red};
          font-size: 0.75rem;
        `

      default:
        return `
          color: ${theme.palette.grey[800]};
          font-family: ${theme.font.secondary};
          font-size: 1rem;
          font-weight: 400;`
    }
  }}
`

export default Typography
