import React from 'react'
import { ComponentMeta, ComponentStory } from '@storybook/react'

import GridView from '@app/components/molecules/GridView/GridView'
import { SimpleCell } from '@app/components/atoms/DataGrid/DataGrid'
import { OrderDirection } from '@app/utils/constants'

const defaultColumns = [
  {
    id: 'id',
    title: 'id',
    Cell: SimpleCell,
    isSortable: true,
  },
  {
    id: 'fruit',
    title: 'Fruit',
    Cell: SimpleCell,
    isSortable: true,
  },
  {
    id: 'vegetables',
    title: 'Vegetables',
    Cell: SimpleCell,
    isSortable: true,
  },
]

const defaultData = [
  {
    id: 1,
    fruit: 'Apple',
    vegetables: 'Pepper',
  },
  {
    id: 2,
    fruit: 'Pear',
    vegetables: 'Cucumber',
  },
]

export const Basic: ComponentStory<typeof GridView> = ({
  isLoading = false,
  error = null,
  data = defaultData,
  columns = defaultColumns,
  orderBy = 'id',
  orderDirection = OrderDirection.Asc,
  ...props
}) => {
  return (
    <GridView
      isLoading={isLoading}
      error={error}
      data={data}
      columns={columns}
      orderBy={orderBy}
      orderDirection={orderDirection}
      {...props}
    />
  )
}

export default {
  title: '2 Molecules/GridView',
  component: GridView,
  parameters: {
    docs: {
      source: {
        type: 'code',
      },
    },
  },
} as ComponentMeta<typeof GridView>
