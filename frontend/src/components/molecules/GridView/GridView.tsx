import React, { useMemo } from 'react'
import styled from 'styled-components'

import Grid from '@material-ui/core/Grid'
import CircularProgress from '@material-ui/core/CircularProgress'

import EmptyListMessage from '@app/components/atoms/EmptyListMessage/EmptyListMessage'

import { getErrorMessage } from '@app/utils/errorHandling'
import { OrderDirection } from '@app/utils/constants'

import DataGrid, {
  DataGridProps,
} from '@app/components/atoms/DataGrid/DataGrid'

export type OrderBy<ItemType> = keyof ItemType

export interface GridViewProps<ItemType extends { id: number }>
  extends Pick<
    DataGridProps<ItemType>,
    | 'columns'
    | 'getCollapsedContainer'
    | 'collapsedRowId'
    | 'onNextDataRequested'
  > {
  data: DataGridProps<ItemType>['data'] | null
  isLoading: boolean
  error: unknown
  total: number | null
  orderDirection: OrderDirection | null
  orderBy: OrderBy<ItemType>
  noDataContent?: JSX.Element
  errorContent?: JSX.Element
  defaultOrderBy?: string
  onSortChange: (sort: {
    orderBy: OrderBy<ItemType>
    orderDirection: OrderDirection
  }) => void
}

const GridView = <ItemType extends { id: number }>({
  isLoading,
  error,
  data,
  columns,
  total,
  onNextDataRequested,
  orderBy,
  orderDirection,
  onSortChange,
  getCollapsedContainer,
  collapsedRowId,
  noDataContent,
  errorContent,

  defaultOrderBy = 'id',
}: GridViewProps<ItemType>): JSX.Element => {
  const sort = useMemo(() => {
    if (!orderBy || !orderDirection) {
      return null
    }

    return {
      direction: orderDirection.toLowerCase() as 'asc' | 'desc',
      id: orderBy as string,
    }
  }, [orderBy, orderDirection])

  const errorMessage = useMemo(
    () =>
      getErrorMessage(error, {
        404: 'Not Found',
        default: 'Internal Server Error',
      }),
    [error],
  )

  const handleSortChange: DataGridProps<ItemType>['onSortChange'] = (
    nextSort,
  ) => {
    console.log(nextSort, 'nextSort')
    if (!nextSort) {
      return onSortChange({
        orderBy: defaultOrderBy as keyof ItemType,
        orderDirection: OrderDirection.Desc,
      })
    }

    onSortChange({
      orderBy: nextSort.id as keyof ItemType,
      orderDirection: nextSort.direction.toUpperCase() as OrderDirection,
    })
  }

  if (error) {
    return (
      <EmptyListMessageContainer>
        {errorContent ?? <EmptyListMessage title={errorMessage ?? undefined} />}
      </EmptyListMessageContainer>
    )
  }

  if (!data && isLoading) {
    return (
      <LoaderContainer container alignItems="center" justifyContent="center">
        <CircularProgress size={20} />
      </LoaderContainer>
    )
  }

  if (!data || !data.length) {
    return (
      <EmptyListMessageContainer>
        {noDataContent ?? <EmptyListMessage />}
      </EmptyListMessageContainer>
    )
  }

  return (
    <>
      <DataGrid<ItemType>
        getCollapsedContainer={getCollapsedContainer}
        collapsedRowId={collapsedRowId}
        columns={columns}
        data={data}
        hasMore={Boolean(total && data.length < total)}
        onNextDataRequested={onNextDataRequested}
        sort={sort}
        onSortChange={handleSortChange}
      />
    </>
  )
}

const LoaderContainer = styled(Grid)`
  height: 3rem;
`

const EmptyListMessageContainer = styled.div`
  margin: 4rem auto;
  display: flex;
  justify-content: center;
`

export default GridView
