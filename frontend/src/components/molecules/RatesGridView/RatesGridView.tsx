import React, { useEffect } from 'react'
import { CellProps } from 'react-table'
import { Box } from '@material-ui/core'

import Button from '@app/components/atoms/Button/Button'
import { DataGridColumn } from '@app/components/atoms/DataGrid/DataGrid'

import { RateDto } from '@app/dto/rates.dto'
import { Routes } from '@app/utils/enums'
import { resolveRouteNamespace } from '@app/utils/routingUtils'
import GridView, {
  GridViewProps,
} from '@app/components/molecules/GridView/GridView'
import Typography from '@app/components/atoms/Typography/Typography'

export interface RatesGridViewProps
  extends Omit<GridViewProps<RateDto>, 'columns'> {
  onGetInitialData: () => void
}

const RatesGridView = ({
  onGetInitialData,
  ...props
}: RatesGridViewProps): JSX.Element => {
  const columns = React.useMemo<DataGridColumn<RateDto>[]>(
    () => [
      {
        id: 'code',
        title: 'Code',
        Cell: CodeCell,
        isSortable: true,
      },
      {
        id: 'country',
        title: 'Country',
        Cell: CountryCell,
        isSortable: true,
      },
      {
        id: 'currency',
        title: 'Currency',
        Cell: CurrencyCell,
        isSortable: true,
      },
      {
        id: 'amount',
        title: 'Amount',
        Cell: AmountCell,
        isSortable: true,
      },
      {
        id: 'rate',
        title: 'Rate',
        Cell: RateCell,
        isSortable: true,
      },

      {
        id: 'actions',
        Cell: ActionsCell,
      },
    ],
    [],
  )

  useEffect(() => {
    onGetInitialData()
  }, [])

  return <GridView<RateDto> columns={columns} {...props} />
}

const CodeCell = ({ cell }: CellProps<RateDto, unknown>) => {
  return <Typography variant="content">{cell.row.original.code}</Typography>
}

const CountryCell = ({ cell }: CellProps<RateDto, unknown>) => {
  return <Typography variant="content">{cell.row.original.country}</Typography>
}

const CurrencyCell = ({ cell }: CellProps<RateDto, unknown>) => {
  return <Typography variant="content">{cell.row.original.currency}</Typography>
}

const AmountCell = ({ cell }: CellProps<RateDto, unknown>) => {
  return <Typography variant="content">{cell.row.original.amount}</Typography>
}

const RateCell = ({ cell }: CellProps<RateDto, unknown>) => {
  return <Typography variant="content">{cell.row.original.rate}</Typography>
}

const ActionsCell = ({ cell }: CellProps<RateDto, unknown>) => {
  return (
    <Box display="flex" alignItems="center" justifyContent="center">
      <Button
        inverted
        onClick={() => {
          resolveRouteNamespace(Routes.RateDetail, {
            rateCode: cell.row.original.code,
          })
        }}
      >
        Detail
      </Button>
    </Box>
  )
}

export default RatesGridView
