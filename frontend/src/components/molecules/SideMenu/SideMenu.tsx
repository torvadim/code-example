import React from 'react'
import styled from 'styled-components'
import { Box } from '@mui/material'
import { Link, navigate } from 'gatsby'
import { useDispatch, useSelector } from 'react-redux'

import Typography from '@app/components/atoms/Typography/Typography'
import Button from '@app/components/atoms/Button/Button'
import Logo from '@app/components/atoms/Logo/Logo'

import { theme } from '@app/theme'
import { selectUserInfo } from '@app/store/core/userInfo/userInfo.selectors'
import { logoutAction } from '@app/store/core/auth/auth.actions'
import { Routes } from '@app/utils/enums'

const SideMenu = () => {
  const dispatch = useDispatch()
  const userInfo = useSelector(selectUserInfo)

  const onLogoutClick = () => {
    dispatch(logoutAction())

    navigate(Routes.Login)
  }

  return (
    <StyledBox
      width="6.875rem"
      display="flex"
      flexDirection="column"
      position="fixed"
      height="100vh"
    >
      <Box
        display="flex"
        justifyContent="center"
        alignItems="center"
        my={4}
        color={theme.palette.common.white}
      >
        <Link to="/">
          <StyledLogo width="60px" />
        </Link>
      </Box>

      <OverflowContainer>
        <PrimaryMenuContainer>
          <StyledLink to="/">
            <MenuItem>Rates</MenuItem>
          </StyledLink>
          <StyledLink to="/">
            <MenuItem>Not Implemented</MenuItem>
          </StyledLink>
        </PrimaryMenuContainer>
      </OverflowContainer>
      <SecondaryMenuContainer>
        <StyledTypography>{userInfo?.login}</StyledTypography>
        <StyledButton onClick={onLogoutClick}>Logout</StyledButton>
      </SecondaryMenuContainer>
    </StyledBox>
  )
}

const OverflowContainer = styled.div`
  overflow: hidden;
  position: relative;
  flex: 1 1 auto;

  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`

const MenuItem = styled(Typography)`
  color: ${({ theme }) => theme.palette.common.white};
  padding: 1rem;
  width: 100%;
  border-top: 1px solid white;
`

const StyledLink = styled(Link)`
  width: 100%;
  text-decoration: none;
`

const StyledButton = styled(Button)`
  margin: 1rem 0;
`

const StyledTypography = styled(Typography)`
  color: ${({ theme }) => theme.palette.common.white};
  margin: 1rem 0;
`

const StyledLogo = styled(Logo)`
  color: ${({ theme }) => theme.palette.common.white};
`
const StyledBox = styled(Box)`
  background-color: #00396f;

  max-width: 100px;
`

const PrimaryMenuContainer = styled.div`
  flex: 5 1 auto;
  display: flex;
  flex-direction: column;

  align-items: center;
`

const SecondaryMenuContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: flex-end;

  flex: 1 0 auto;
`

export default SideMenu
