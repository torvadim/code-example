import React, { useState, SyntheticEvent } from 'react'
import styled from 'styled-components'
import { ComponentStory, ComponentMeta } from '@storybook/react'

import Box from '@material-ui/core/Box'

import TextField from '@app/components/atoms/TextField/TextField'
import Typography from '@app/components/atoms/Typography/Typography'
import Button from '@app/components/atoms/Button/Button'

import CredentialsLayout, {
  SidebarAction,
} from '@app/components/organisms/CredentialsLayout/CredentialsLayout'

export default {
  title: '3 Organisms/CredentialsLayout',
  component: CredentialsLayout,
} as ComponentMeta<typeof CredentialsLayout>

export const Basic: ComponentStory<typeof CredentialsLayout> = ({
  sidebarTitle = 'Welcome to code example',
  mainTitle = 'Login',
  ...props
}) => {
  const sidebarActions: SidebarAction[] = [
    { id: 'register', label: 'Register' },
    { id: 'lostPassword', label: 'Lost Password' },
  ]

  const [username, setUsername] = useState('')
  const [password, setPassword] = useState('')

  const onLoginFormSubmit = (event: SyntheticEvent) => {
    event.preventDefault()
  }

  return (
    <CredentialsLayout
      sidebarTitle={sidebarTitle}
      sidebarContent={<Typography>Credentials layout</Typography>}
      sidebarActions={sidebarActions}
      onSidebarActionButtonClick={(sidebarAction: SidebarAction) =>
        console.log({ sidebarAction })
      }
      mainTitle={mainTitle}
      mainContent={
        <StyledForm onSubmit={onLoginFormSubmit}>
          <Box mt={3}>
            <TextField
              required
              fullWidth
              size="large"
              label="Login"
              variant="secondary"
              value={username}
              onChange={(event) => setUsername(event.target.value)}
            />
          </Box>
          <Box my={5}>
            <TextField
              required
              fullWidth
              size="large"
              label="Password"
              variant="secondary"
              type="password"
              value={password}
              onChange={(event) => setPassword(event.target.value)}
            />
          </Box>
          <StyledButton size="large" type="submit">
            Login
          </StyledButton>
        </StyledForm>
      }
      {...props}
    />
  )
}

export const Loading: ComponentStory<typeof CredentialsLayout> = ({
  sidebarTitle = 'Loading application',
  ...props
}) => {
  return <CredentialsLayout sidebarTitle={sidebarTitle} loading {...props} />
}

const StyledForm = styled.form`
  display: flex;
  flex-direction: column;
  width: 20rem;
`

const StyledButton = styled(Button)`
  width: 8.75rem;
`
