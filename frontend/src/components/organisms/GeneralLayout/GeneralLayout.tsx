import React, { ReactNode } from 'react'
import styled from 'styled-components'

import Grid from '@material-ui/core/Grid'
import Box from '@material-ui/core/Box'

import Typography from '@app/components/atoms/Typography/Typography'
import LoadingSpinner from '@app/components/atoms/LoadingSpinner/LoadingSpinner'
import Logo from '@app/components/atoms/Logo/Logo'
import SideMenu from '@app/components/molecules/SideMenu/SideMenu'

interface GeneralLayoutProps {
  children: ReactNode
  title: string
  loading?: boolean
  className?: string
}

export interface IMenuItem {
  id: string
  icon: JSX.Element
  label?: string
  href?: string
  as?: string
  testId?: string
}

const GeneralLayout = ({
  children,
  title,
  loading,
  className,
}: GeneralLayoutProps): JSX.Element => {
  return (
    <LayoutGrid container className={className}>
      <StyledSideMenu />
      <Box
        pr={4}
        py={4}
        pl="calc(6.875rem + 30px)"
        display="flex"
        flexDirection="column"
        flex={1}
      >
        <Box my={1}>
          <StyledTypography variant="heading">{title}</StyledTypography>
        </Box>

        {loading ? (
          <Box
            display="flex"
            justifyContent="center"
            alignItems="center"
            width={1}
            flex={1}
          >
            <LoadingSpinner loading>
              <Logo width="75px" />
            </LoadingSpinner>
          </Box>
        ) : (
          children
        )}
      </Box>
    </LayoutGrid>
  )
}

const StyledSideMenu = styled(SideMenu)`
  z-index: 2;
`

const LayoutGrid = styled(Grid)`
  min-height: 100vh;
`

const StyledTypography = styled(Typography)`
  font-weight: 600;
`

export default GeneralLayout
