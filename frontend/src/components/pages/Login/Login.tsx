import React, { useEffect } from 'react'
import styled from 'styled-components'
import { navigate, RouteComponentProps } from '@reach/router'
import { useSnackbar } from 'notistack'
import { Controller, useForm } from 'react-hook-form'
import { useDispatch } from 'react-redux'
import { Box } from '@material-ui/core'
import * as yup from 'yup'

import Typography from '@app/components/atoms/Typography/Typography'
import TextField from '@app/components/atoms/TextField/TextField'
import Button from '@app/components/atoms/Button/Button'

import { LocalStorageKeys } from '@app/utils/constants'
import CredentialsLayout, {
  SidebarAction,
} from '@app/components/organisms/CredentialsLayout/CredentialsLayout'
import { Routes } from '@app/utils/enums'
import { api } from '@app/utils/api/api'
import { getUserInfoAction } from '@app/store/core/userInfo/userInfo.actions'
import { useYupValidationResolver } from '@app/hooks/useYupValidationResolver'

interface LoginFormData {
  login: string
  password: string
}

const validationSchema = yup.object({
  login: yup.string().required(),
  password: yup.string().required(),
})

export const LoginPage = (props: RouteComponentProps): JSX.Element => {
  const dispatch = useDispatch()
  const { enqueueSnackbar } = useSnackbar()

  const validationResolver = useYupValidationResolver(validationSchema)

  const { handleSubmit, formState, control } = useForm<LoginFormData>({
    resolver: validationResolver,
    mode: 'onChange',
  })

  const sidebarActions: SidebarAction[] = [
    {
      id: Routes.Register,
      label: 'Register',
    },
  ]

  useEffect(() => {
    const authToken = localStorage.getItem(LocalStorageKeys.AuthToken)

    if (authToken) {
      navigate(Routes.Index)
    }
  }, [])

  const onLogin = async ({ login, password }: LoginFormData) => {
    try {
      const { data } = await api.login(login, password)

      localStorage.setItem(LocalStorageKeys.AuthToken, data.access_token)

      dispatch(getUserInfoAction())

      return navigate(Routes.Index)
    } catch (error) {
      enqueueSnackbar('Error ocurred!', {
        variant: 'error',
      })
    }
  }

  return (
    <CredentialsLayout
      sidebarTitle="Welcome"
      mainTitle="Login page"
      sidebarContent={<Typography>Welcome to the login page</Typography>}
      sidebarActions={sidebarActions}
      onSidebarActionButtonClick={(sidebarAction: SidebarAction) =>
        navigate(sidebarAction.id)
      }
      mainContent={
        <StyledMainBox display="flex" flexDirection="column">
          <form onSubmit={handleSubmit(onLogin)}>
            <Box mt={2}>
              <Controller<LoginFormData>
                name="login"
                control={control}
                render={({ field: { name, onChange, onBlur, value } }) => (
                  <TextField
                    name={name}
                    value={value}
                    onChange={onChange}
                    onBlur={onBlur}
                    label="Login"
                    fullWidth
                    size="large"
                    error={!!formState.errors.login}
                  />
                )}
              />
            </Box>
            <Box mt={2}>
              <Controller<LoginFormData>
                name="password"
                control={control}
                render={({ field: { name, onChange, onBlur, value } }) => (
                  <TextField
                    name={name}
                    value={value}
                    onChange={onChange}
                    onBlur={onBlur}
                    label="Password"
                    type="password"
                    fullWidth
                    size="large"
                    error={!!formState.errors.password}
                  />
                )}
              />
            </Box>
            <StyledButton
              variant="contained"
              disabled={!formState.isValid}
              type="submit"
            >
              Login
            </StyledButton>
          </form>
        </StyledMainBox>
      }
      {...props}
    />
  )
}

const StyledMainBox = styled(Box)`
  max-width: 20rem;
`

const StyledButton = styled(Button)`
  width: 9.4rem;
  height: 3.1rem;
  margin-top: 3.8rem;
`

export default LoginPage
