import React from 'react'
import { RouteComponentProps } from '@reach/router'
import { useDispatch, useSelector } from 'react-redux'
import GeneralLayout from '@app/components/organisms/GeneralLayout/GeneralLayout'
import Typography from '@app/components/atoms/Typography/Typography'
import RatesGridView from '@app/components/molecules/RatesGridView/RatesGridView'
import {
  GetRatesListAction,
  RatesSortAction,
} from '@app/store/core/rates/rates.actions'

import {
  selectRatesData,
  selectRatesError,
  selectRatesLoading,
  selectRatesOrderBy,
  selectRatesOrderDirection,
} from '@app/store/core/rates/rates.selectors'

const RatesPage = (props: RouteComponentProps) => {
  const dispatch = useDispatch()

  const data = useSelector(selectRatesData)
  const orderBy = useSelector(selectRatesOrderBy)
  const orderDirection = useSelector(selectRatesOrderDirection)
  const error = useSelector(selectRatesError)
  const isLoading = useSelector(selectRatesLoading)

  return (
    <GeneralLayout title="Rates" {...props}>
      <Typography>Placeholder</Typography>
      <RatesGridView
        data={data}
        error={error}
        total={data?.length ?? 0}
        orderDirection={orderDirection}
        isLoading={isLoading}
        orderBy={orderBy}
        onSortChange={(sortProperties) => {
          console.log(sortProperties, 'sortProperties')
          dispatch(RatesSortAction(sortProperties))
        }}
        onGetInitialData={() => {
          dispatch(GetRatesListAction())
        }}
      />
    </GeneralLayout>
  )
}

export default RatesPage
