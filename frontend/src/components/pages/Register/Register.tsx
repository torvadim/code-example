import React, { useEffect } from 'react'
import styled from 'styled-components'
import { navigate, RouteComponentProps } from '@reach/router'
import { useSnackbar } from 'notistack'
import { Controller, useForm } from 'react-hook-form'
import { Box } from '@material-ui/core'
import * as yup from 'yup'

import Typography from '@app/components/atoms/Typography/Typography'
import TextField from '@app/components/atoms/TextField/TextField'
import Button from '@app/components/atoms/Button/Button'

import CredentialsLayout, {
  SidebarAction,
} from '@app/components/organisms/CredentialsLayout/CredentialsLayout'
import { Routes } from '@app/utils/enums'
import { api } from '@app/utils/api/api'
import { useYupValidationResolver } from '@app/hooks/useYupValidationResolver'
import { LocalStorageKeys } from '@app/utils/constants'

interface RegisterFormData {
  login: string
  password: string
  confirmPassword: string
}

const validationSchema = yup.object({
  login: yup.string().required(),
  password: yup.string().required(),
  confirmPassword: yup
    .string()
    .required()
    .oneOf([yup.ref('password')], 'Passwords does not match'),
})

export const RegisterPage = (props: RouteComponentProps): JSX.Element => {
  const { enqueueSnackbar } = useSnackbar()

  const validationResolver = useYupValidationResolver(validationSchema)

  const { handleSubmit, formState, control } = useForm<RegisterFormData>({
    resolver: validationResolver,
    mode: 'onChange',
  })

  const sidebarActions: SidebarAction[] = [
    {
      id: Routes.Login,
      label: 'Login',
    },
  ]

  const onSubmit = async ({ login, password }: RegisterFormData) => {
    try {
      await api.register(login, password)

      enqueueSnackbar('User registered!', {
        variant: 'success',
      })

      return navigate(Routes.Login)
    } catch (error) {
      enqueueSnackbar('Error ocurred!', {
        variant: 'error',
      })
    }
  }

  useEffect(() => {
    const authToken = localStorage.getItem(LocalStorageKeys.AuthToken)

    if (authToken) {
      navigate(Routes.Index)
    }
  }, [])

  return (
    <CredentialsLayout
      sidebarTitle="Registration"
      mainTitle="Registration"
      sidebarContent={<Typography>Already have an account? Login!</Typography>}
      sidebarActions={sidebarActions}
      onSidebarActionButtonClick={(sidebarAction: SidebarAction) =>
        navigate(sidebarAction.id)
      }
      mainContent={
        <StyledMainBox display="flex" flexDirection="column">
          <form onSubmit={handleSubmit(onSubmit)}>
            <Box mt={2}>
              <Controller<RegisterFormData>
                name="login"
                control={control}
                render={({ field: { name, onChange, onBlur, value } }) => (
                  <TextField
                    name={name}
                    value={value}
                    onChange={onChange}
                    onBlur={onBlur}
                    label="Username"
                    fullWidth
                    size="large"
                    error={!!formState.errors.login}
                  />
                )}
              />
            </Box>
            <Box mt={2}>
              <Controller<RegisterFormData>
                name="password"
                control={control}
                render={({ field: { name, onChange, onBlur, value } }) => (
                  <TextField
                    name={name}
                    value={value}
                    onChange={onChange}
                    onBlur={onBlur}
                    label="Password"
                    type="password"
                    fullWidth
                    size="large"
                    error={!!formState.errors.password}
                  />
                )}
              />
            </Box>
            <Box mt={2}>
              <Controller<RegisterFormData>
                name="confirmPassword"
                control={control}
                render={({ field: { name, onChange, onBlur, value } }) => (
                  <TextField
                    name={name}
                    value={value}
                    onChange={onChange}
                    onBlur={onBlur}
                    label="Password again"
                    type="password"
                    fullWidth
                    size="large"
                    error={!!formState.errors.password}
                  />
                )}
              />
              {formState.errors?.confirmPassword && (
                <Typography variant="errorText">
                  {formState.errors.confirmPassword?.message}
                </Typography>
              )}
            </Box>
            <StyledButton
              variant="contained"
              disabled={!formState.isValid}
              type="submit"
            >
              Register
            </StyledButton>
          </form>
        </StyledMainBox>
      }
      {...props}
    />
  )
}

const StyledMainBox = styled(Box)`
  max-width: 20rem;
`

const StyledButton = styled(Button)`
  width: 9.4rem;
  height: 3.1rem;
  margin-top: 3.8rem;
`

export default RegisterPage
