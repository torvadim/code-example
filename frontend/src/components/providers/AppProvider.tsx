import React from 'react'
import { setAutoFreeze } from 'immer'
import { WrapPageElementBrowserArgs } from 'gatsby'
import { SnackbarProvider } from 'notistack'
import { ThemeProvider } from 'styled-components'
import { Provider as ReduxProvider } from 'react-redux'
import { PersistGate } from 'redux-persist/integration/react'

import store, { persistor } from '@app/store'

import { ThemeProvider as MuiThemeProvider } from '@material-ui/core/styles'
import CssBaseline from '@material-ui/core/CssBaseline'

import GlobalStyleProvider from '@app/components/providers/GlobalStyleProvider'
import { theme } from '@app/theme'
import LoadingContainer from '@app/components/molecules/LoadingContainer/LoadingContainer'
import NotificationsProvider from '@app/components/providers/NotificationsProvider'

setAutoFreeze(false)

const AppProvider = ({
  element,
  children,
}: WrapPageElementBrowserArgs): JSX.Element => {
  return (
    <ReduxProvider store={store}>
      <MuiThemeProvider theme={theme}>
        <ThemeProvider theme={theme}>
          <GlobalStyleProvider>
            <PersistGate persistor={persistor} loading={<LoadingContainer />}>
              <SnackbarProvider
                hideIconVariant
                maxSnack={5}
                anchorOrigin={{
                  vertical: 'top',
                  horizontal: 'right',
                }}
              >
                <NotificationsProvider>
                  {element ?? children}
                </NotificationsProvider>
              </SnackbarProvider>

              <CssBaseline />
            </PersistGate>
          </GlobalStyleProvider>
        </ThemeProvider>
      </MuiThemeProvider>
    </ReduxProvider>
  )
}

export default AppProvider
