import React, { ReactNode, useEffect } from 'react'
import { useSnackbar } from 'notistack'
import { useDispatch, useSelector } from 'react-redux'

import { removeNotificationAction } from '@app/store/ui/notifications/notifications.actions'
import { selectNotifications } from '@app/store/ui/notifications/notifications.selectors'

interface NotificationsProviderProps {
  children: ReactNode
}

const NotificationsProvider = ({
  children,
}: NotificationsProviderProps): JSX.Element => {
  const { enqueueSnackbar } = useSnackbar()
  const dispatch = useDispatch()

  const notifications = useSelector(selectNotifications)

  useEffect(() => {
    notifications.forEach((notification) => {
      enqueueSnackbar(notification.text, {
        variant: notification.type,
      })

      dispatch(removeNotificationAction(notification.id))
    })
  }, [notifications])

  return <>{children}</>
}

export default NotificationsProvider
