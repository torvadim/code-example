export interface RateDto {
  id: number
  code: string
  country: string
  currency: string
  amount: number
  rate: string
}
