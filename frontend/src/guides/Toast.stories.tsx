import React from 'react'
import { ComponentMeta } from '@storybook/react'
import { useSnackbar, VariantType } from 'notistack'

import Button from '@app/components/atoms/Button/Button'

interface ToastContainerProps {
  type: VariantType
  message: string
}

// @see https://github.com/storybookjs/storybook/issues/13304#issuecomment-745360896
export const Basic = ({
  message = 'Default toast message',
  type = 'success',
}: ToastContainerProps): JSX.Element => {
  const { enqueueSnackbar } = useSnackbar()

  const onButtonClick = () => {
    enqueueSnackbar(message, {
      variant: type,
    })
  }

  return <Button onClick={onButtonClick}>Display toast</Button>
}

export default {
  title: '1 Atoms/Toast',
  component: Basic,
  parameters: {
    docs: {
      source: {
        type: 'code',
      },
      description: {
        component: 'Extends [notistack](https://iamhosseindhv.com/notistack)',
      },
    },
  },
} as ComponentMeta<typeof Basic>
