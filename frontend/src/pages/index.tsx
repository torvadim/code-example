import React, { useEffect } from 'react'
import { Routes } from '@app/utils/enums'
import { Router, useLocation } from '@reach/router'
import { useDispatch } from 'react-redux'
import Loadable from 'react-loadable'

import { getUserInfoAction } from '@app/store/core/userInfo/userInfo.actions'
import LoadingContainer from '@app/components/molecules/LoadingContainer/LoadingContainer'
import { LocalStorageKeys } from '@app/utils/constants'
import { navigate } from 'gatsby'

const Login = Loadable({
  loader: () => import('@app/components/pages/Login/Login'),
  loading: LoadingContainer,
})

const Register = Loadable({
  loader: () => import('@app/components/pages/Register/Register'),
  loading: LoadingContainer,
})

const Rates = Loadable({
  loader: () => import('@app/components/pages/Rates/Rates'),
  loading: LoadingContainer,
})

const IndexPage = (): JSX.Element => {
  const dispatch = useDispatch()
  const location = useLocation()

  const pathnameBlacklist = [Routes.Login, Routes.Register]

  const isBlacklistedPage = pathnameBlacklist.includes(
    location.pathname as Routes,
  )

  useEffect(() => {
    if (isBlacklistedPage) {
      return
    }

    if (!localStorage.getItem(LocalStorageKeys.AuthToken)) {
      navigate(Routes.Login)

      return
    }

    dispatch(getUserInfoAction())
  }, [])

  return (
    <>
      <Router>
        <Rates path={Routes.Index}></Rates>
        <Login path={Routes.Login} />
        <Register path={Routes.Register} />
      </Router>
    </>
  )
}

export default IndexPage
