import { action } from 'typesafe-actions'

import { AuthActionTypes } from '@app/store/core/auth/auth.constants'

export const logoutAction = () => action(AuthActionTypes.Logout)

export const logoutSuccessAction = () => action(AuthActionTypes.LogoutSuccess)

export const logoutFailureAction = (error: unknown) =>
  action(AuthActionTypes.LogoutFailure, error)

export const logoutFromAllDevicesAction = (token: string) =>
  action(AuthActionTypes.LogoutFromAllDevices, { token })

export const logoutFromAllDevicesSuccessAction = () =>
  action(AuthActionTypes.LogoutFromAllDevicesSuccess)

export const logoutFromAllDevicesFailureAction = (error: unknown) =>
  action(AuthActionTypes.LogoutFromAllDevicesFailure, error)
