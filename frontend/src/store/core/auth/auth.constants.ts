export enum AuthActionTypes {
  Logout = '[core] [auth] Logout]',
  LogoutSuccess = '[core] [auth] [LogoutSuccess]',
  LogoutFailure = '[core] [auth] [LogoutFailure]',

  LogoutFromAllDevices = '[core] [auth] LogoutFromAllDevices]',
  LogoutFromAllDevicesSuccess = '[core] [auth] [LogoutFromAllDevicesSuccess]',
  LogoutFromAllDevicesFailure = '[core] [auth] [LogoutFromAllDevicesFailure]',
}
