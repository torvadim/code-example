import { call, put, takeLatest } from 'redux-saga/effects'
import { navigate } from '@reach/router'

import { api } from '@app/utils/api/api'
import { getErrorMessage } from '@app/utils/getErrorMessage'
import { AuthActionTypes } from '@app/store/core/auth/auth.constants'
import { addNotificationAction } from '@app/store/ui/notifications/notifications.actions'
import { LocalStorageKeys } from '@app/utils/constants'
import { Routes } from '@app/utils/enums'

import * as actions from '@app/store/core/auth/auth.actions'
import * as userInfoActions from '@app/store/core/userInfo/userInfo.actions'

function* logoutSaga() {
  try {
    yield call(api.logout)

    localStorage.removeItem(LocalStorageKeys.AuthToken)

    yield put(userInfoActions.resetUserInfoAction())

    yield put(
      addNotificationAction({
        text: 'Success!',
        type: 'success',
      }),
    )

    navigate(Routes.Login)

    yield put(actions.logoutSuccessAction())
  } catch (error) {
    const errorMessageKey = getErrorMessage(error, {
      401: 'Unauthorized',
      404: 'NotFound',
      default: 'Oops, something bad happened',
    })

    if (errorMessageKey) {
      yield put(
        addNotificationAction({
          text: errorMessageKey,
          type: 'error',
        }),
      )
    }

    yield put(actions.logoutFailureAction(error))
  }
}

function* logoutFromAllDevicesSaga(
  action: ReturnType<typeof actions.logoutFromAllDevicesAction>,
) {
  try {
    yield call(api.logoutFromAllSessions, action.payload.token)

    yield put(
      addNotificationAction({
        text: 'Logged out!',
        type: 'success',
      }),
    )

    yield put(actions.logoutFromAllDevicesSuccessAction())
  } catch (error) {
    const errorMessageKey = getErrorMessage(error, {
      401: 'Unauthorized',
      404: 'NotFound',
      default: 'Oops, something bad happened',
    })

    if (errorMessageKey) {
      yield put(
        addNotificationAction({
          text: 'Oops',
          type: 'error',
        }),
      )
    }

    yield put(actions.logoutFromAllDevicesFailureAction(error))
  }
}

export default function* watchAuthSaga(): Generator {
  yield takeLatest(AuthActionTypes.Logout, logoutSaga)

  yield takeLatest(
    AuthActionTypes.LogoutFromAllDevices,
    logoutFromAllDevicesSaga,
  )
}
