import { action } from 'typesafe-actions'

import { RatesActionTypes } from '@app/store/core/rates/rates.constants'
import { RateDto } from '@app/dto/rates.dto'
import { RatesSortProperties } from '@app/store/core/rates/rates.types'

export const GetRatesListAction = () => action(RatesActionTypes.GetRatesList)

export const GetRatesListSuccessAction = (response: RateDto[]) =>
  action(RatesActionTypes.GetRatesListSuccess, response)

export const GetRatesListFailureAction = (error: unknown) =>
  action(RatesActionTypes.GetRatesListFailure, error)

export const GetImportsAction = () => action(RatesActionTypes.GetImports)

export const GetImportsSuccessAction = () =>
  action(RatesActionTypes.GetImportsSuccess)

export const GetImportFailuresAction = (error: unknown) =>
  action(RatesActionTypes.GetImportsFailure, error)

export const RatesSortAction = (sort: RatesSortProperties) =>
  action(RatesActionTypes.RatesSort, sort)
