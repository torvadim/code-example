export enum RatesActionTypes {
  GetRatesList = '[core] [rates] [GetRatesList]',
  GetRatesListSuccess = '[core] [rates] [GetRatesListSuccess]',
  GetRatesListFailure = '[core] [rates] [GetRatesListFailure]',

  RatesSort = '[core] [rates] [RatesSort]',

  GetImports = '[core] [rates] [GetImports]',
  GetImportsSuccess = '[core] [rates] [GetImportsSuccess]',
  GetImportsFailure = '[core] [rates] [GetImportsFailure]',
}
