import { ActionType } from 'typesafe-actions'
import produce, { castDraft } from 'immer'

import * as ratesActions from '@app/store/core/rates/rates.actions'

import { BaseDataStore } from '@app/types'
import { RateDto } from '@app/dto/rates.dto'
import { RatesActionTypes } from '@app/store/core/rates/rates.constants'
import { OrderDirection } from '@app/utils/constants'

type RateAction = ActionType<typeof ratesActions>

export interface RatesStore extends BaseDataStore<RateDto[]> {
  isImportLoading: boolean
  importError: unknown
  orderBy: keyof RateDto
  orderDirection: OrderDirection
}

const initialRatesState: RatesStore = {
  error: null,
  isLoading: false,
  orderBy: 'id',
  isImportLoading: false,
  importError: null,
  data: null,
  orderDirection: OrderDirection.Desc,
}

const ratesReducer = produce<RatesStore, [RateAction]>((state, action) => {
  switch (action.type) {
    case RatesActionTypes.GetRatesList:
      state.isLoading = true
      state.data = null
      state.error = null

      break

    case RatesActionTypes.GetRatesListSuccess:
      state.isLoading = false
      state.data = castDraft(action.payload)
      state.error = null

      break

    case RatesActionTypes.GetImportsFailure:
      state.isLoading = false
      state.error = action.payload
      state.data = null

      break

    case RatesActionTypes.GetImports:
      state.isImportLoading = true

      break

    case RatesActionTypes.GetImportsSuccess:
      state.isImportLoading = false

      break

    case RatesActionTypes.GetImportsFailure:
      state.isImportLoading = false
      state.importError = action.payload

      break

    case RatesActionTypes.RatesSort:
      state.orderBy = action.payload.orderBy

      if (action.payload.orderDirection) {
        state.orderDirection = action.payload.orderDirection
      }

      state.data =
        state.data &&
        state.data.sort((a, b) => {
          if (action.payload.orderDirection === OrderDirection.Asc) {
            return a[action.payload.orderBy] > b[action.payload.orderBy]
              ? -1
              : 1
          }

          return a[action.payload.orderBy] > b[action.payload.orderBy] ? 1 : -1
        })

      break

    default:
      return state
  }
}, initialRatesState)

export default ratesReducer
