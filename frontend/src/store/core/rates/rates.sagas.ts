import { call, put, takeLatest } from 'redux-saga/effects'

import * as ratesActions from '@app/store/core/rates/rates.actions'
import { getErrorMessage } from '@app/utils/getErrorMessage'
import { api } from '@app/utils/api/api'
import { addNotificationAction } from '@app/store/ui/notifications/notifications.actions'
import { RatesActionTypes } from '@app/store/core/rates//rates.constants'

function* getRatesSaga() {
  try {
    const { data } = yield call(api.getRatesList)

    yield put(ratesActions.GetRatesListSuccessAction(data))
  } catch (error) {
    const errorMessageKey = getErrorMessage(error, {
      401: 'Unauthorized',
      404: 'NotFound',
      default: 'Oops, something bad happened',
    })

    if (errorMessageKey) {
      yield put(
        addNotificationAction({
          text: errorMessageKey,
          type: 'error',
        }),
      )
    }

    yield put(ratesActions.GetRatesListFailureAction(error))
  }
}

function* getImportsSaga() {
  try {
    yield call(api.getImports)

    yield put(ratesActions.GetImportsSuccessAction())
  } catch (error) {
    const errorMessageKey = getErrorMessage(error, {
      401: 'Unauthorized',
      404: 'NotFound',
      default: 'Oops, something bad happened',
    })

    if (errorMessageKey) {
      yield put(
        addNotificationAction({
          text: errorMessageKey,
          type: 'error',
        }),
      )
    }

    yield put(ratesActions.GetImportFailuresAction(error))
  }
}

export default function* watchRatesSaga(): Generator {
  yield takeLatest(RatesActionTypes.GetRatesList, getRatesSaga)

  yield takeLatest(RatesActionTypes.GetImports, getImportsSaga)
}
