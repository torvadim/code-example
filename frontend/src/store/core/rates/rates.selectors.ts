import { RootState } from '@app/store'

export const selectRatesData = (store: RootState) => store.core.rates.data

export const selectRatesLoading = (store: RootState) =>
  store.core.rates.isLoading

export const selectRatesError = (store: RootState) => store.core.rates.error

export const selectImportLoading = (store: RootState) =>
  store.core.rates.isImportLoading

export const selectImportError = (store: RootState) =>
  store.core.rates.importError

export const selectRatesOrderBy = (store: RootState) => store.core.rates.orderBy

export const selectRatesOrderDirection = (store: RootState) =>
  store.core.rates.orderDirection
