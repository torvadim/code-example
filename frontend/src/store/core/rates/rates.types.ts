import { RateDto } from '@app/dto/rates.dto'
import { OrderDirection } from '@app/utils/constants'

export interface RatesSortProperties {
  orderDirection: OrderDirection | null
  orderBy: keyof RateDto
}
