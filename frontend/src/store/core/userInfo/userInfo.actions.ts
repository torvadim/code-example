import { action } from 'typesafe-actions'

import { UserInfoActionTypes } from '@app/store/core/userInfo/userInfo.constants'

export const getUserInfoAction = () => action(UserInfoActionTypes.GetUserInfo)

export const getUserInfoSuccessAction = (response: { login: string }) =>
  action(UserInfoActionTypes.GetUserInfoSuccess, response)

export const getUserInfoFailureAction = (error: unknown) =>
  action(UserInfoActionTypes.GetUserInfoFailure, error)

export const resetUserInfoAction = () =>
  action(UserInfoActionTypes.ResetUserInfoState)
