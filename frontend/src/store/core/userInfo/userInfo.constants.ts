export enum UserInfoActionTypes {
  GetUserInfo = '[core] [userInfo] [GetUserInfo]',
  GetUserInfoSuccess = '[core] [userInfo] [GetUserInfoSuccess]',
  GetUserInfoFailure = '[core] [userInfo] [GetUserInfoFailure]',

  ResetUserInfoState = '[core] [userInfo] [ResetUserInfoState]',
}
