import { ActionType } from 'typesafe-actions'
import produce, { castDraft } from 'immer'

import * as userInfoActions from '@app/store/core/userInfo/userInfo.actions'

import { UserInfoActionTypes } from '@app/store/core/userInfo/userInfo.constants'
import { UserDto } from '@app/dto/user.dto'
import { BaseDataStore } from '@app/types'

type UserInfoAction = ActionType<typeof userInfoActions>

export interface UserInfoStore extends BaseDataStore<UserDto> {}

const initialUserInfoState: UserInfoStore = {
  error: null,
  isLoading: false,
  data: null,
}

const userInfoReducer = produce<UserInfoStore, [UserInfoAction]>(
  (state, action) => {
    switch (action.type) {
      case UserInfoActionTypes.GetUserInfo:
        state.isLoading = true
        state.data = null
        state.error = null

        break

      case UserInfoActionTypes.GetUserInfoSuccess:
        state.isLoading = false
        state.data = castDraft(action.payload)
        state.error = null

        break

      case UserInfoActionTypes.GetUserInfoFailure:
        state.isLoading = false
        state.error = action.payload
        state.data = null

        break

      case UserInfoActionTypes.ResetUserInfoState:
        return initialUserInfoState

      default:
        return state
    }
  },
  initialUserInfoState,
)

export default userInfoReducer
