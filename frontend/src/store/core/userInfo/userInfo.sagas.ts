import { call, put, takeLatest } from 'redux-saga/effects'

import { api } from '@app/utils/api/api'
import { UserInfoActionTypes } from '@app/store/core/userInfo/userInfo.constants'
import { addNotificationAction } from '@app/store/ui/notifications/notifications.actions'

import * as actions from '@app/store/core/userInfo/userInfo.actions'
import { getErrorMessage } from '@app/utils/getErrorMessage'

function* getUserInfoSaga() {
  try {
    const { data } = yield call(api.getMyUser)

    yield put(actions.getUserInfoSuccessAction(data))
  } catch (error) {
    const errorMessageKey = getErrorMessage(error, {
      401: 'Unauthorized',
      404: 'NotFound',
      default: 'Oops, something bad happened',
    })

    if (errorMessageKey) {
      yield put(
        addNotificationAction({
          text: errorMessageKey,
          type: 'error',
        }),
      )
    }

    yield put(actions.getUserInfoFailureAction(error))
  }
}

export default function* watchUserInfoSaga(): Generator {
  yield takeLatest(UserInfoActionTypes.GetUserInfo, getUserInfoSaga)
}
