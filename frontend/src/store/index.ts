import { StateType } from 'typesafe-actions'
import { createStore, combineReducers, applyMiddleware } from 'redux'
import { composeWithDevTools } from 'redux-devtools-extension/logOnlyInProduction'
import { persistStore, persistReducer } from 'redux-persist'
import { all } from 'redux-saga/effects'
import storage from 'redux-persist/lib/storage'
import createSagaMiddleware from 'redux-saga'

import watchUserInfoSaga from '@app/store/core/userInfo/userInfo.sagas'
import userInfoReducer from '@app/store/core/userInfo/userInfo.reducer'
import ratesReducer from '@app/store/core/rates/rates.reducer'
import notificationsReducer from '@app/store/ui/notifications/notifications.reducer'
import watchAuthSaga from '@app/store/core/auth/auth.sagas'
import watchRatesSaga from '@app/store/core/rates/rates.sagas'

const coreReducer = combineReducers({
  userInfo: userInfoReducer,
  rates: ratesReducer,
})

const uiReducer = combineReducers({
  notifications: notificationsReducer,
})

const corePersistConfig = {
  key: 'core',
  storage,
  whiteList: ['userInfo', 'userOperators'],
}

const rootReducer = combineReducers({
  core: persistReducer(corePersistConfig, coreReducer),

  ui: uiReducer,
})

const sagaMiddleware = createSagaMiddleware()

const store = createStore(
  rootReducer,
  composeWithDevTools(applyMiddleware(sagaMiddleware)),
)

export type RootStore = StateType<typeof store>

export type RootState = StateType<typeof rootReducer>

const sagas = [watchUserInfoSaga(), watchAuthSaga(), watchRatesSaga()]

function* rootSaga(): Generator {
  yield all(sagas)
}

sagaMiddleware.run(rootSaga)

export const persistor = persistStore(store)

export default store
