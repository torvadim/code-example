export enum NotificationActionTypes {
  AddNotification = '[ui] [notifications] [AddNotification]',
  RemoveNotification = '[ui] [notifications] [RemoveNotification]',
}
