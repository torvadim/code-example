import { RootState } from '@app/store'

export const selectNotifications = (store: RootState) =>
  store.ui.notifications.messages
