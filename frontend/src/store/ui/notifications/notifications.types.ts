import { VariantType } from 'notistack'

export interface Notification {
  id?: string
  type: VariantType
  text: string
}
