export interface BaseDataStore<DataType> {
  error: unknown
  isLoading: boolean
  data: DataType | null
}
