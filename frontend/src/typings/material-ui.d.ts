// @see https://material-ui.com/guides/typescript/#customization-of-theme

import { Theme as BaseTheme } from '@material-ui/core/styles/createTheme'

import { Theme as CustomTheme } from '@app/theme'

declare module '@material-ui/core/styles/createTheme' {
  interface Theme extends CustomTheme, BaseTheme {}

  interface ThemeOptions extends CustomTheme, BaseTheme {}
}
