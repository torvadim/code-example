// @see https://styled-components.com/docs/api#typescript

import 'styled-components'

import { Theme as BaseTheme } from '@material-ui/core/styles/createTheme'

declare module 'styled-components' {
  export interface DefaultTheme extends BaseTheme {}
}
