import axios, { AxiosResponse } from 'axios'
import { navigate } from 'gatsby'
import { LocalStorageKeys } from '@app/utils/constants'
import { Routes } from '@app/utils//enums'
import { UserDto } from '@app/dto/user.dto'
import { RateDto } from '@app/dto/rates.dto'

enum ApiRoutes {
  Login = 'auth/login',
  Refresh = 'auth/refresh',
  Logout = 'auth/logout',
  LogoutFromAllSessions = 'auth/logout-from-all-sessions',
  Me = 'user/me',
  Register = 'user/register',
  Delete = 'user/delete',
  Rates = '/rates',
  Imports = '/rates/imports',
}

const GATSBY_BASE_API_URL = process.env.GATSBY_BASE_API_URL
const STORYBOOK_BASE_API_URL = process.env.STORYBOOK_BASE_API_URL

const instance = axios.create({
  baseURL: GATSBY_BASE_API_URL ?? STORYBOOK_BASE_API_URL,
})

const createAxiosRequestInterceptor = () => {
  return instance.interceptors.request.use(
    (config) => {
      const token = localStorage.getItem(LocalStorageKeys.AuthToken)

      if (!token && config.url !== ApiRoutes.Login) {
        navigate(Routes.Login)

        return config
      }

      config.headers.Authorization = 'Bearer ' + token

      return config
    },
    (error) => Promise.reject(error),
  )
}

const createAxiosResponseInterceptor = () => {
  const responseInterceptor = instance.interceptors.response.use(
    (config) => config,
    async (error) => {
      if (
        error.response.status !== 401 ||
        error.config.url === ApiRoutes.Login
      ) {
        return Promise.reject(error)
      }

      instance.interceptors.response.eject(responseInterceptor)

      try {
        const { data } = await instance.post(ApiRoutes.Refresh)

        const { access_token } = data

        axios.defaults.headers.common['Authorization'] =
          'Bearer ' + access_token

        localStorage.setItem(LocalStorageKeys.AuthToken, access_token)

        const originalRequestResult = await instance(error.config)

        return originalRequestResult
      } catch (error) {
        localStorage.removeItem(LocalStorageKeys.AuthToken)

        navigate(Routes.Login)
      } finally {
        createAxiosResponseInterceptor()
      }
    },
  )

  return responseInterceptor
}

createAxiosResponseInterceptor()
createAxiosRequestInterceptor()

type Response<Data = unknown> = Promise<AxiosResponse<Data>>

export const api = {
  login: (
    login: string,
    password: string,
  ): Response<{ access_token: string }> => {
    return instance.post(ApiRoutes.Login, { login, password })
  },

  register: (login: string, password: string): Response<void> => {
    return instance.post(ApiRoutes.Register, { login, password })
  },

  logout: (): Response<void> => {
    return instance.post(ApiRoutes.Logout)
  },

  logoutFromAllSessions: (token: string): Response<void> => {
    return instance.post(ApiRoutes.LogoutFromAllSessions, { token })
  },

  getMyUser: (): Response<UserDto> => {
    return instance.get(ApiRoutes.Me)
  },

  getRatesList: (): Response<RateDto[]> => {
    return instance.get(ApiRoutes.Rates)
  },

  getImports: (): Response<void> => {
    return instance.get(ApiRoutes.Imports)
  },
}
