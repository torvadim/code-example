export enum LocalStorageKeys {
  AuthToken = 'authToken',
}

export enum OrderDirection {
  Asc = 'ASC',
  Desc = 'DESC',
}
