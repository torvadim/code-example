export function prop<Props, Key extends keyof Props>(targetProp: Key) {
  return (props: Props): Props[Key] => props[targetProp]
}
