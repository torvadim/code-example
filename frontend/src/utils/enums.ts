export enum Routes {
  Index = '/',
  Login = '/login',
  Register = '/register',
  Rates = '/rates',
  RateDetail = '/rates/:rateCode',
}
