import { Routes } from '@app/utils/enums'

export const resolveRouteNamespace = (
  namespace: Routes,
  routeParams: { [key: string]: string },
) => {
  const transformedNamespace = Object.entries(routeParams).reduce<string>(
    (acc, [key, value]) => acc.replace(`:${key}`, value),
    namespace,
  )

  if (transformedNamespace.includes(':')) {
    throw new Error(
      `Invalid namespace '${transformedNamespace}'. Make sure you provide all dynamic values.`,
    )
  }

  return transformedNamespace
}
